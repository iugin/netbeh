LABEL_COLUMN = 'labels'
LABELS = [
    [0, "Unknown"],
    [5, "DNS"],
    [7, "HTTP"],
    [8, "MDNS"],
    [9, "NTP"],
    [12, "SSDP"],
    [18, "DHCP"],
    [81, "ICMP"],
    [82, "IGMP"],
    [91, "TLS"],
    [100, "SIP"],
    [102, "ICMPV6"],
    [119, "Facebook"],
    [120, "Twitter"],
    [122, "GMail"],
    [124, "YouTube"],
    [125, "Skype"],
    [126, "Google"],
    [137, "Generic Protocol"],
    [142, "Whatsapp"],
    [156, "Spotify"],
    [157, "Messenger"],
    [178, "Amazon"],
    [185, "Telegram"],
    [189, "Whatsapp Voice"],
    [196, "DNS over HTTPS"],
    [211, "Instagram"],
    [212, "Microsoft"],
    [219, "Office365"],
    [220, "CloudFlare"],
    [228, "PlayStore"],
    [239, "GoogleServices"]
]
N_LABELS = len(LABELS)
N_FEATURES = 200
N_HISTORY = 6
