import csv
import numpy as np
import sys
import time
import warnings
from watchdog.events import FileSystemEventHandler
from watchdog.observers import Observer

warnings.filterwarnings('ignore', category=FutureWarning) # Ignore certain warnings
import tensorflow as tf
import LSTM as lstm


def on_file_update(path, event_handler):
    observer = Observer()
    observer.schedule(event_handler, path, recursive=True)
    observer.start()
    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()
    observer.join()


class NNEventHandler(FileSystemEventHandler):
    """Listen for every time the file is modified and predicts its labels."""

    # Create a new untrained model
    model = lstm.create_model()

    def __init__(self, checkpoint_dir):
        # Loads the weights
        latest_checkpoint = tf.train.latest_checkpoint(checkpoint_dir)
        self.model.load_weights(latest_checkpoint)

    def on_moved(self, event):
        return

    def on_created(self, event):
        return

    def on_deleted(self, event):
        return

    def on_modified(self, event):
        if not event.is_directory and event.src_path.endswith("-NN.csv"):
            try:
                # read a text file as a list of lines
                # find the last line, change to a file you have
                f = open(event.src_path)
                line_list = f.readlines()
                f.close()
                lines = list(csv.reader(line_list[-6:]))

                # Extract the features and labels columns
                features = np.array(lines)
                features = features[:, :-1].astype(np.float32)
                features = np.reshape(features, [1, 6, 200])

                # Predict the labels
                labels = self.model(features)

                result = "Inferred device behaviour"
                separator = ": "
                import Config as conf
                for i in range(0, conf.N_LABELS):
                    if labels[0][i] > 0.5:
                        result = result + separator + conf.LABELS[i][1]
                        separator = ", "

                print(result)
            except:
                print("Something went wrong, lines not evaluated.")


if __name__ == "__main__":
    path = sys.argv[1] if len(sys.argv) > 1 else exit(1)
    checkpoint_dir = sys.argv[2] if len(sys.argv) > 2 else exit(1)
    event_handler = NNEventHandler(checkpoint_dir)
    on_file_update(path, event_handler)


