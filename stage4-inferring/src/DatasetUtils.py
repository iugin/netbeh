import math
import numpy as np
import os.path
import pandas

import Config as conf


def load_dataset(file_path):
    """
    Loads the dataset from the specified path. The dataset must be a CSV.
    :param file_path: the path to the csv
    :return: the couple (features, labels)
    """

    def labels_to_one_hot(labels_set, all_labels=None):
        """
        It maps the labels concatenated in form of string into a one-hot array.
        This function must be called for each record of the dataset labels.
        :param labels_set: the set of labels specified in this record as concatenation of strings
        :param all_labels: the full set of labels (optional)
        :return: an array of bytes (1 or 0), one for each label
        """
        if all_labels is None:
            all_labels = np.array(conf.LABELS)[:, 0].astype(np.int32)
        ground_truth = np.zeros(len(all_labels), dtype=np.int32)
        if labels_set:
            idx = 0
            labels_set = list(map(int, labels_set.split(";")))
            for label in all_labels:
                if label in labels_set:
                    ground_truth[idx] = 1.0
                idx += 1
        return ground_truth

    if os.path.isfile(file_path):
        # Load CSV using Pandas
        dataset = pandas.read_csv(file_path, dtype={conf.LABEL_COLUMN: str})
        # Extract the features and labels columns
        features = dataset.iloc[:, :-1]
        labels = dataset.iloc[:, -1]
        labels.fillna('', inplace=True)
        # Convert the multi-class labels to one-hot vector
        labels = labels.map(labels_to_one_hot)
        # Convert both to numpy arrays
        features = np.array(features, dtype=np.float32)
        labels = np.array(list(labels), dtype=np.bool)
        return features, labels


def load_dataset_list(file_path_list):
    return [load_dataset(path) for path in file_path_list]


def load_dataset_list_from_folder(folder_path):
    from os import walk
    dataset_list = [os.path.join(dirpath, name) for (dirpath, dirnames, filenames) in walk(folder_path) for name in filenames]
    return load_dataset_list(dataset_list)


def preprocess_dataset_for_lstm(features, labels):
    """
    Preprocess the data before the LSTM model.
    """

    def compress_labels(labels_set):
        """
        It compresses all the labels of a batch using a logical or
        :param labels_set: the list of labels sets
        :return: the final label set
        """
        ground_truth = np.full(conf.N_LABELS, False)
        for l in labels_set:
            ground_truth = np.logical_or(ground_truth, l)
        return ground_truth * 1

    def take_last_label(labels_set):
        """
        It compresses all the labels of a batch by taking only the ones relative to the last batch.
        :param labels_set: the list of labels sets
        :return: the final label set
        """
        return labels_set[len(labels_set)-1] * 1

    def extract_random_batches(f, l, batch_size):
        """
        Extracts from the dataset (features+labels) a subset of items of size: batch_size
        :param f: the features set
        :param l: the labels set
        :param batch_size: the size of the batch
        :return: the new dataset (features+labels)
        """
        if len(f) >= batch_size and len(l) >= batch_size:
            if len(f) == batch_size:
                n_batches = 1
            else:
                n_batches = math.trunc((len(f)-batch_size)/2)
            chosen_batches = range(0, n_batches)
            r_features = [f[i:i+batch_size, :] for i in chosen_batches]
            r_labels = [l[i:i+batch_size, :] for i in chosen_batches]
            return np.asarray(r_features), np.asarray(r_labels)
        else:
            return np.empty([0, conf.N_HISTORY, conf.N_FEATURES]), np.empty([0, conf.N_HISTORY, conf.N_LABELS])

    # Reshape according to the history batches
    features, labels = extract_random_batches(features, labels, conf.N_HISTORY)
    # Compress the labels for each batch
    # labels = np.array([compress_labels(label) for label in labels])
    labels = np.array([take_last_label(label) for label in labels])
    return features, labels


def preprocess_dataset_for_lstm_list(dataset_list):
    sets = [preprocess_dataset_for_lstm(f, l) for (f, l) in dataset_list]
    sets = [(f, l) for (f, l) in sets if f.size > 0]
    features = np.concatenate([x[0] for x in sets], axis=0)
    labels = np.concatenate([x[1] for x in sets], axis=0)
    return features, labels
