# Import tensorflow
from __future__ import absolute_import, division, print_function, unicode_literals

import Config as conf
# External files
import DatasetUtils as du
import sklearn.model_selection as sk
import warnings
# Utilities
from datetime import datetime

warnings.filterwarnings('ignore', category=FutureWarning) # Ignore certain warnings
# Tensorflow
import tensorflow as tf

tf.enable_eager_execution()
print("TensorFlow version: {}".format(tf.__version__))
print("Eager execution: {}".format(tf.executing_eagerly()))


def create_model():
    # ----------- RNN Model (LSTM) -----------
    # TODO could use better function
    #  e.g. https://stats.stackexchange.com/questions/181/how-to-choose-the-number-of-hidden-layers-and-nodes-in-a-feedforward-neural-netw/136542#136542
    hidden_nodes = int(2 / 3 * (conf.N_HISTORY * conf.N_FEATURES))
    # print(f"The number of hidden nodes is {hidden_nodes}.")
    model = tf.keras.Sequential([
        tf.keras.layers.LSTM(hidden_nodes, return_sequences=False, input_shape=(conf.N_HISTORY, conf.N_FEATURES)),
        tf.keras.layers.Dense(conf.N_LABELS, activation=tf.nn.sigmoid)
    ])
    model.compile(optimizer='adam',
                  loss='binary_crossentropy',
                  metrics=['accuracy'])
    return model


def load_dataset(base_path):
    """
    Loads all the dataset present in the folder
    :base_path: the path where the dataset are located
    :return: train_features, test_features, train_labels, test_labels
    """
    # ----------- Load the dataset -----------
    dataset = du.load_dataset_list_from_folder(base_path)
    # Preprocess the dataset
    dataset_features, dataset_labels = du.preprocess_dataset_for_lstm_list(dataset)
    # Split train and test
    return sk.train_test_split(dataset_features,
                               dataset_labels,
                               test_size=0.33,
                               random_state=42)


def train_model(model, train_features, test_features, train_labels, test_labels, checkpoint_path, n_epoch=10):
    # +++ CALLBACKS +++
    # Tensorboard visualization
    log_dir = checkpoint_path + "/logs/scalars/" + datetime.now().strftime("%Y%m%d-%H%M%S")
    tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir)

    # Create a callback that saves the model's weights every 5 epochs
    cp_callback = tf.keras.callbacks.ModelCheckpoint(
        filepath=checkpoint_path,
        verbose=1,
        save_weights_only=True,
        period=2)

    # +++ TRAINING +++
    print("Training ...")
    history = model.fit(
        train_features,  # input
        train_labels,  # output
        epochs=n_epoch,
        validation_data=(
            test_features,
            test_labels
        ),
        callbacks=[tensorboard_callback, cp_callback]
    )
    return history

