import numpy as np
import os
import sys
import warnings

warnings.filterwarnings('ignore', category=FutureWarning) # Ignore certain warnings
import tensorflow as tf
import LSTM as lstm


def plot_train_history(history):
    from matplotlib import pyplot
    # plot loss during training
    pyplot.subplot(211)
    pyplot.title('Loss')
    pyplot.plot(history.history['loss'], label='train')
    pyplot.plot(history.history['val_loss'], label='test')
    pyplot.legend()
    # plot accuracy during training
    pyplot.subplot(212)
    pyplot.title('Accuracy')
    pyplot.plot(history.history['acc'], label='train')
    pyplot.plot(history.history['val_acc'], label='test')
    pyplot.legend()
    pyplot.show()


if __name__ == "__main__":
    dataset_dir = sys.argv[1] if len(sys.argv) > 1 else exit(1)
    checkpoint_dir = sys.argv[2] if len(sys.argv) > 2 else exit(1)
    n_epoch = int(sys.argv[3]) if len(sys.argv) > 3 else 10

    # +++ MODEL_CHECKPOINTS +++
    # Include the epoch in the file name (uses `str.format`)
    checkpoint_path = checkpoint_dir + "/cp-{epoch:04d}.ckpt"
    checkpoint_dir = os.path.dirname(checkpoint_path)

    # Create the model
    model = lstm.create_model()
    # Load the dataset
    train_features, test_features, train_labels, test_labels = lstm.load_dataset(dataset_dir)
    # Train the model
    history = lstm.train_model(
        model,
        train_features,
        test_features,
        train_labels,
        test_labels,
        checkpoint_path,
        n_epoch
    )

    print("Average test loss: ", np.average(history.history['loss']))
    # plot_train_history(history)

    # Evaluate the trained model
    loss, acc = model.evaluate(test_features, test_labels, verbose=2)
    print("Trained model, accuracy: {:5.2f}%, loss: {:5.2f}%".format(100*acc, 100*loss))

    # Create a new untrained model
    model = lstm.create_model()

    # Evaluate the untrained model
    loss, acc = model.evaluate(test_features, test_labels, verbose=2)
    print("Untrained model, accuracy: {:5.2f}%, loss: {:5.2f}%".format(100*acc, 100*loss))

    # Loads the weights
    latest_checkpoint = tf.train.latest_checkpoint(checkpoint_dir)
    model.load_weights(latest_checkpoint)

    # Re-evaluate the model
    loss, acc = model.evaluate(test_features, test_labels, verbose=2)
    print("Restored model, accuracy: {:5.2f}%, loss: {:5.2f}%".format(100*acc, 100*loss))
