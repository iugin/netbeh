package it.unibo.netbeh.core.engine

import it.unibo.netbeh.core.behaviour.AtomicBehaviour
import it.unibo.netbeh.core.properties.PropertiesPacket

trait Stage2[I<:PropertiesPacket, O<:AtomicBehaviour] extends Stage[I, O] {

}
