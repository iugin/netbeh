package it.unibo.netbeh.core.engine

import it.unibo.netbeh.core.behaviour.{AtomicBehaviour, DeviceBehaviour}

trait Stage3[I<:AtomicBehaviour, O<:DeviceBehaviour[I]] extends Stage[I, Seq[O]] {

}
