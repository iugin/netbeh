package it.unibo.netbeh.core.behaviour.extensions

import it.unibo.netbeh.core.behaviour.{Behaviour, ExpectedBehaviour}

/**
  * It is the extension for the expected behaviour that checks the counters;
  * it is directly connected with CounterExtension.
  */
trait ExpectedCounterExtension extends ExpectedBehaviour[Behaviour with CounterExtension] with CounterExtension {
  this: Behaviour =>

  def maxIncomingLengthVariance: Int

  def minIncomingLengthVariance: Int

  def avgIncomingLengthVariance: Double

  def totalIncomingLengthVariance: Long

  def maxOutgoingLengthVariance: Int

  def minOutgoingLengthVariance: Int

  def avgOutgoingLengthVariance: Double

  def totalOutgoingLengthVariance: Long

  def nIncomingPacketsVariance: Long

  def nOutgoingPacketsVariance: Long

  override def isRespectedBy(behaviour: Behaviour with CounterExtension): Boolean =
    super.isRespectedBy(behaviour) &&
      (behaviour.maxIncomingLength - maxIncomingLength).abs <= maxIncomingLengthVariance &&
      (behaviour.minIncomingLength - minIncomingLength).abs <= minIncomingLengthVariance &&
      (behaviour.avgIncomingLength - avgIncomingLength).abs <= avgIncomingLengthVariance &&
      (behaviour.totalIncomingLength - totalIncomingLength).abs <= totalIncomingLengthVariance &&
      (behaviour.maxOutgoingLength - maxOutgoingLength).abs <= maxOutgoingLengthVariance &&
      (behaviour.minOutgoingLength - minOutgoingLength).abs <= minOutgoingLengthVariance &&
      (behaviour.avgOutgoingLength - avgOutgoingLength).abs <= avgOutgoingLengthVariance &&
      (behaviour.totalOutgoingLength - totalOutgoingLength).abs <= totalOutgoingLengthVariance &&
      (behaviour.nIncomingPackets - nIncomingPackets).abs <= nIncomingPacketsVariance &&
      (behaviour.nOutgoingPackets - nOutgoingPackets).abs <= nOutgoingPacketsVariance
}
