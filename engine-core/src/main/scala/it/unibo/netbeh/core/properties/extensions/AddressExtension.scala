package it.unibo.netbeh.core.properties.extensions

import it.unibo.netbeh.core.properties.PropertiesPacket

/**
  * Used to extend the PropertiesPacket functionalities.
  * It provides information regarding the source and destination address and port.
  */
trait AddressExtension {
  this: PropertiesPacket =>

  /**
    * The source address of the packet.
    */
  var sourceHostAddress: String

  /**
    * The source host name of the packet if present, otherwise its host address.
    */
  var sourceHostName: String

  /**
    * The source port from which it has been sent.
    */
  var sourcePort: Int

  /**
    * The destination address of the packet.
    */
  var destinationHostAddress: String

  /**
    * The destination host name of the packet if present, otherwise its host address.
    */
  var destinationHostName: String

  /**
    * The destination port of the packet.
    */
  var destinationPort: Int
}
