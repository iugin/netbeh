package it.unibo.netbeh.core.behaviour.extensions

import java.sql.Timestamp

import it.unibo.netbeh.core.behaviour.Behaviour

/**
  * Used to extend the AtomicBehaviour functionalities.
  * It provides the set of time counter.
  */
trait TimeExtension {
  this: Behaviour =>

  /**
    * The timestamp of the first received packet.
    */
  def timestamp: Timestamp
}
