package it.unibo.netbeh.core.properties

/**
  * The basic PropertiesPacket.
  * It must be implemented in order to be used.
  * It is possible to extend its functionalities by exploiting multiple inheritance with its extensions.
  */
trait PropertiesPacket {

  /**
    * When called, it extracts all the properties and executes all the computations.
    * It should always call "super" when overridden.
    * Every computations should be done inside it (for efficiency reasons) instead of directly initializing every variable immediately.
    * @return true if the computation if successful, else otherwise.
    */
  def extractProperties: Boolean = true
}
