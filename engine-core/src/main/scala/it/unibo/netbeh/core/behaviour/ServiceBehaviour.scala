package it.unibo.netbeh.core.behaviour

trait ServiceBehaviour[A <: AtomicBehaviour] extends Behaviour {

  /**
    * The list of atomic behaviours that compose this service behaviour.
    */
  var behaviours: Seq[A]

  /**
    * Add an atomic behaviour to this service behaviour.
    *
    * @param atomicBehaviour the atomic behaviour to add; it must have the same hostName of the name of the service.
    */
  def merge(atomicBehaviour: A): Unit
}

object ServiceBehaviour {

  def apply[A <: AtomicBehaviour](service: String, behaviours: Seq[A] = List()): ServiceBehaviour[A] = new DefaultServiceBehaviour(service, behaviours)

  def apply[A <: AtomicBehaviour](service: String, atomicBehaviour: A): ServiceBehaviour[A] = ServiceBehaviour(service, List(atomicBehaviour))

  def merge[A <: AtomicBehaviour, S <: ServiceBehaviour[A]](s: S, a: A): S = {
    s.behaviours.find(_ == a) match {
      // If this behaviour already exists, merge it
      case Some(b) => b + a
      // If this is a new behaviour, add it
      case None => s.behaviours = a +: s.behaviours
    }
    s
  }

  class DefaultServiceBehaviour[A <: AtomicBehaviour](
                                                       val serviceName: String,
                                                       var behaviours: Seq[A]
                                                     ) extends ServiceBehaviour[A] {

    override def merge(atomicBehaviour: A): Unit = ServiceBehaviour.merge(this, atomicBehaviour)

    override def equals(obj: Any): Boolean = obj.isInstanceOf[ServiceBehaviour[A]] && obj.asInstanceOf[ServiceBehaviour[A]].serviceName == this.serviceName
  }

}
