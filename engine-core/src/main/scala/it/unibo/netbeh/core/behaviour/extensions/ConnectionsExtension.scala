package it.unibo.netbeh.core.behaviour.extensions

import it.unibo.netbeh.core.behaviour.Behaviour

/**
  * Used to extend the AtomicBehaviour functionalities.
  * It provides a set of variables used to memorize the connection parameters.
  */
trait ConnectionsExtension {
  this: Behaviour =>

  /**
    * The port opened on the remote server.
    */
  var servicePort: Int

  /**
    * The set of ports opened on the local device.
    */
  var devicePorts: Seq[Int]
}
