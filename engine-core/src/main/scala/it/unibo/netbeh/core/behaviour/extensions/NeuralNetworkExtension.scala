package it.unibo.netbeh.core.behaviour.extensions

import it.unibo.netbeh.core.behaviour.Behaviour

/**
  * Used to extend the AtomicBehaviour functionalities.
  * It provides a set of variables used to memorize the connection parameters.
  */
trait NeuralNetworkExtension {
  this: Behaviour =>

  /**
    * The value into which it is remapped the service.
    * It is used to discretize the number of services to give as input to the neural network
    */
  def serviceNumber: Int
}
