package it.unibo.netbeh.core.behaviour

/**
  * Base structure of a behaviour.
  */
trait Behaviour {

  /**
    * The service name.
    * It identifies the type of service provided (its representation human-readable).
    * @return the service name (the domain name, an hardcoded string, MAC address, IP address and port...)
    */
  def serviceName: String
}
