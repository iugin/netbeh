package it.unibo.netbeh.core.behaviour

/**
  * This interface represents the generic expected behaviour of any kind.
  * Every class that wants to be used to check the expectation of a behaviour
  * should implement it (either directly or indirectly).
  * @tparam B the type of behaviour that it wraps
  */
trait ExpectedBehaviour[B] extends Behaviour {

  /**
    * This method check if the behaviour given respects the expectations or not.
    * @param behaviour the behaviour to compare
    * @return true if it is wrapped, false otherwise
    */
  def isRespectedBy(behaviour: B): Boolean = true
}
