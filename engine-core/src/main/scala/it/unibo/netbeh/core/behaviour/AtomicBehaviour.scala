package it.unibo.netbeh.core.behaviour

/**
  * The basic AtomicBehaviour.
  * It must be implemented in order to be used.
  * It is possible to extend its functionalities by exploiting multiple inheritance with its extensions.
  */
trait AtomicBehaviour extends Behaviour {

  /**
    * The service address.
    * It identifies uniquely the behaviour and its current instance.
    * @return the service address (it may be the IP address and port, MAC address...)
    */
  def serviceAddress: String

  /**
    * Merge 2 Behaviours into one and returns it
    * @param behaviour the second behaviour
    * @return the new Behaviour containing the merged values
    */
  def +[A <: AtomicBehaviour](behaviour: A): AtomicBehaviour
}
