package it.unibo.netbeh.core.behaviour

/**
  * This interface represents the expected behaviour composed by many patterns that can occur.
  * @tparam B the type of Behaviour to verify
  * @tparam EB the type of ExpectedBehaviour check
  */
trait ExpectedComplexBehaviour[B <: Behaviour, EB <: ExpectedBehaviour[B]] extends Behaviour {

  /**
    * The sequence of pattern that could occur
    */
  def patterns: Seq[ExpectedPatternWindowBehaviour[B, EB]]

  /**
    * Check if the sequence respects the pattern specified
    * @param behaviours the sequence of behaviour to validate against the pattern
    * @return true if it respects the pattern
    */
  def validate(behaviours: Seq[B]): Boolean
}
