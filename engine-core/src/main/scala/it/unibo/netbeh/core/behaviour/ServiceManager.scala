package it.unibo.netbeh.core.behaviour

/**
  * Ensemble of utilities used to manage the service abstraction.
  * Any operation that requires any kind of function regarding the service, should be implemented here.
  * @tparam A
  * @tparam S
  * @tparam D
  */
trait ServiceManager[A<:AtomicBehaviour, S<:ServiceBehaviour[A], D<:DeviceBehaviour[A]] {

  /**
    * Compare an AtomicBehaviour with a ServiceBehaviour and returns true if the AtomicBehaviour is an instance of that service.
    * @param a the AtomicBehaviour
    * @param s the ServiceBehaviour
    * @return true if the AtomicBehaviour is an instance of the ServiceBehaviour
    */
  def hasService(a: A)(s: S): Boolean

  /**
    * Computes the most logical name for the service containing that behaviour.
    * @param a the first AtomicBehaviour in the service
    * @return the name for the ServiceBehaviour
    */
  def serviceName(a: A): String
}
