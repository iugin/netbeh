package it.unibo.netbeh.core.behaviour.extensions

import it.unibo.netbeh.core.behaviour.Behaviour

import scala.util.Try

/**
  * Used to extend the AtomicBehaviour functionalities.
  * It provides a set of counter used to compute statistics of the behaviour.
  */
trait CounterExtension {
  this: Behaviour =>

  /**
    * The maximum size of the packets received.
    */
  def maxIncomingLength: Int

  /**
    * The minimum size of the packets received.
    */
  def minIncomingLength: Int

  /**
    * The average size of the packets received.
    */
  def avgIncomingLength: Double = Try[Double](totalIncomingLength/nIncomingPackets).recover{ case _: ArithmeticException => 0D}.get

  /**
    * The total amount of information received.
    */
  def totalIncomingLength: Long

  /**
    * The maximum size of the packets sent.
    */
  def maxOutgoingLength: Int

  /**
    * The minimum size of the packets sent.
    */
  def minOutgoingLength: Int

  /**
    * The average size of the packets sent.
    */
  def avgOutgoingLength: Double = Try[Double](totalOutgoingLength/nOutgoingPackets).recover{ case _: ArithmeticException => 0D}.get

  /**
    * The total amount of information sent.
    */
  def totalOutgoingLength: Long

  /**
    * The number of packets received.
    */
  def nIncomingPackets: Long

  /**
    * The number of packets sent.
    */
  def nOutgoingPackets: Long
}
