package it.unibo.netbeh.core.behaviour

trait WindowBehaviour[B <: Behaviour] {

  /**
    * The number identifier of the scanning window
    */
  def windowId: Int

  /**
    * The list of behaviours detected
    */
  def behaviourList: Seq[B]
}

object WindowBehaviour {
  def apply[B <: Behaviour](windowId: Int, behaviourList: Seq[B]): WindowBehaviour[B] = WindowBehaviourImpl(windowId, behaviourList)

  case class WindowBehaviourImpl[B <: Behaviour](
                                                  windowId: Int,
                                                  behaviourList: Seq[B]
                                                ) extends WindowBehaviour[B]
}
