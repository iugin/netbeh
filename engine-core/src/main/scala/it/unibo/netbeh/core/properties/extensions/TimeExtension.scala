package it.unibo.netbeh.core.properties.extensions

import java.sql.Timestamp

import it.unibo.netbeh.core.properties.PropertiesPacket

/**
  * Used to extend the PropertiesPacket functionalities.
  * It provides all the basic time features.
  */
trait TimeExtension {
  this: PropertiesPacket =>

  /**
    * The time at which the packet has been generated
    */
  def timestamp: Timestamp
}
