package it.unibo.netbeh.core.engine

import rx.lang.scala.Observable

trait Stage[
  I,
  O
] {
  def run(input: Observable[I]): Observable[O]
}
