package it.unibo.netbeh.core.behaviour

/**
  * This class represents a pattern in the sequence of window regarding the behaviour.
  * Every behaviour is subdivided into temporal window, this interface allows to detect
  * patterns into those window sequence.
  *
  * @tparam B  the type of Behaviour to verify
  * @tparam EB the type of expected behaviour to analyze
  */
trait ExpectedPatternWindowBehaviour[B <: Behaviour, EB <: ExpectedBehaviour[B]] {

  /**
    * The sequence of ExpectedBehaviour that must occur in order to validate the pattern.
    */
  def sequence: Seq[EB]

  /**
    * A Map of options characterized by their id and string value.
    * Some example of options could be: only at begin, only at end,
    * once every x windows, once only, ordered or alternated ...
    */
  def options: Map[String, String]
}

object ExpectedPatternWindowBehaviour {

  def apply[B <: Behaviour, EB <: ExpectedBehaviour[B]](sequence: Seq[EB], options: Map[String, String]): ExpectedPatternWindowBehaviour[B, EB] =
    DefaultExpectedPatternWindowBehaviour(sequence, options)

  case class DefaultExpectedPatternWindowBehaviour[B <: Behaviour, EB <: ExpectedBehaviour[B]](
                                                                                                sequence: Seq[EB],
                                                                                                options: Map[String, String]
                                                                                              ) extends ExpectedPatternWindowBehaviour[B, EB]

}
