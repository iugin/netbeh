package it.unibo.netbeh.core.behaviour

import scala.collection.mutable

/**
  * It represents the DeviceBehaviour of a device.
  *
  * @tparam A the type of AtomicBehaviour it handles.
  */
trait DeviceBehaviour[A<:AtomicBehaviour] {

  /**
    * The device name.
    * It may be a name, the IP address and port, the MAC address ...
    */
  def device: String

  /**
    * The collection of service behaviours it is composed of.
    */
  def serviceBehaviours: Seq[_<:ServiceBehaviour[A]]

  /**
    * Add an atomic behaviour to this device behaviour.
    * It is added to a service behaviour or will automatically create the service behaviour to handle it.
    * @param atomicBehaviour the atomic behaviour to add.
    * @return the new DeviceBehaviour updated
    */
  def merge(atomicBehaviour: A): Unit
}

object DeviceBehaviour {

  def apply[A<:AtomicBehaviour](device: String, serviceBehaviours: mutable.Seq[ServiceBehaviour[A]] = mutable.Seq[ServiceBehaviour[A]]())
                               (implicit sm: ServiceManager[A, ServiceBehaviour[A], DeviceBehaviour[A]]): DeviceBehaviour[A] =
    DefaultDeviceBehaviour(
      device,
      serviceBehaviours
    )

  case class DefaultDeviceBehaviour[A<:AtomicBehaviour](
                                                       device: String,
                                                       var serviceBehaviours: mutable.Seq[ServiceBehaviour[A]]
                                                       )(implicit sm: ServiceManager[A, ServiceBehaviour[A], DeviceBehaviour[A]]) extends DeviceBehaviour[A] {

    override def merge(atomicBehaviour: A): Unit = this.serviceBehaviours.find(sm.hasService(atomicBehaviour)) match {
      // If there isn't any service behaviour for this atomic one
      case None => serviceBehaviours = ServiceBehaviour(sm.serviceName(atomicBehaviour), atomicBehaviour) +: serviceBehaviours
      // If there is already a service behaviour for this atomic one
      case Some(s) => s merge atomicBehaviour
    }
  }

}