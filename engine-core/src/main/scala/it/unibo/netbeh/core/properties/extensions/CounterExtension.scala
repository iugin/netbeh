package it.unibo.netbeh.core.properties.extensions

import it.unibo.netbeh.core.properties.PropertiesPacket

/**
  * Used to extend the PropertiesPacket functionalities.
  * It provides all the basic counter regarding the packet.
  */
trait CounterExtension {
  this: PropertiesPacket =>

  /**
    * The size of the packet.
    */
  var length: Int

}
