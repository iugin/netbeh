package it.unibo.netbeh.core.behaviour.extensions

import it.unibo.netbeh.core.behaviour.Behaviour

/**
  * Extension used when we are analyzing multiple devices. It enables the memorization of the device generating the behaviour.
  */
trait DeviceExtension {
  this: Behaviour =>

  /**
    * Device name
    */
  def device: String
}
