package it.unibo.utils

import com.typesafe.config.{Config, ConfigFactory}

/**
  * Object designed to manage all the configuration files to load
  */
object Configuration {

  /**
    * The global generic configuration
    */
  private val config: Config = ConfigFactory.load()

  /**
    * By default returns the global generic configuration
    * @return the configuration
    */
  def apply(): Config = config
}
