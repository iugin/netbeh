package it.unibo.utils

import java.io.{BufferedReader, InputStreamReader}

object SystemUtils {

  /**
    * Executes the given command and returns the string result
    * @param command the command to run
    * @return the string result
    */
  def returnCommandResult(command: String): String = {
    val p = Runtime.getRuntime.exec(command)
    val br: BufferedReader = new BufferedReader(new InputStreamReader(p.getInputStream))
    val r = br.lines().toArray().mkString("\n")
    p.waitFor
    r
  }
}
