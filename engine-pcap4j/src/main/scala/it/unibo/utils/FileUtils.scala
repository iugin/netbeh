package it.unibo.utils

import java.io.File

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule

import scala.collection.JavaConverters._

/**
  * Object that provides utilities for file management
  */
object FileUtils {

  private val mapper = new ObjectMapper()
  mapper.registerModule(DefaultScalaModule)

  /**
    * Clean the file, if exists, by deleting it.
    *
    * @param file the file to clean
    */
  def cleanFile(file: File): Unit = {
    if (file.exists) file.delete
  }

  /**
    * Write to JSON the content given using the default mapper.
    *
    * @param outputFile the file into which to write the data
    * @param content    the content to write
    * @param append     if the content should be appended or not; by default overwrites the file
    */
  def writeToJson[C](outputFile: File, content: C, append: Boolean = false): Unit = {
    // Writing JSON
    content match {
      case s: String => org.apache.commons.io.FileUtils.write(
        outputFile,
        s,
        "UTF-8",
        append
      )

      case _ => org.apache.commons.io.FileUtils.write(
        outputFile,
        mapper.writerWithDefaultPrettyPrinter.writeValueAsString(content),
        "UTF-8",
        append
      )
    }

  }

  /**
    * Like writeToJson but append the content instead of overwriting,
    * it structures the output as an array of json objects.
    *
    * @param outputFile the file into which to write the data
    * @param content    the content to write
    */
  def appendToJsonAsArray[C](outputFile: File, content: C): Unit = {
    if (outputFile.length() == 0) {
      writeToText(outputFile, "[")
    } else {
      def deleteLastChar(file: File): Unit = {
        import java.io.RandomAccessFile
        val f = new RandomAccessFile(file, "rw")
        f.setLength(f.length - 1)
      }
      deleteLastChar(outputFile)
      appendToText(outputFile, ",")
    }
    writeToJson(outputFile, content, append = true)
    appendToText(outputFile, "]")
  }

  /**
    * Write to text the object in input
    *
    * @param outputFile the file into which to write the data
    * @param content    the object to write
    * @param append     if the content should be appended or not; by default overwrites the file
    */
  def writeToText[C](outputFile: File, content: C, append: Boolean = false): Unit = {
    org.apache.commons.io.FileUtils.write(
      outputFile,
      content.toString,
      "UTF-8",
      append
    )
  }

  /**
    * Like writeToText but append the content instead of overwriting
    *
    * @param outputFile the file into which to write the data
    * @param content    the object to write
    */
  def appendToText[C](outputFile: File, content: C): Unit = writeToText(outputFile, content, append = true)

  /**
    * Write to text the sequence in input
    *
    * @param outputFile the file into which to write the data
    * @param content    the sequence of lines
    * @param append     if the content should be appended or not; by default overwrites the file
    */
  def writeLinesToText[C](outputFile: File, content: Seq[C], append: Boolean = false): Unit = {
    org.apache.commons.io.FileUtils.writeLines(
      outputFile,
      "UTF-8",
      content.asJavaCollection,
      append
    )
  }

  /**
    * Like writeLinesToText but append the content instead of overwriting
    *
    * @param outputFile the file into which to write the data
    * @param content    the sequence of lines
    */
  def appendLinesToText[C](outputFile: File, content: Seq[C]): Unit = writeLinesToText(outputFile, content, append = true)
}
