package it.unibo.fxviews

import javafx.application.Platform
import javafx.embed.swing.JFXPanel

/**
  * A trait that gives a way to run tasks on JavaFX Application Thread without overhead
  */
trait FXRunOnUIThread {

  /**
    * A method to run a task on UI thread without overheads
    *
    * @param task the task to run on GUI thread
    */
  def runOnUIThread(task: Runnable): Unit = FXRunOnUIThread.run(task)

}

object FXRunOnUIThread {

  def run(task: Runnable): Unit = {
    new JFXPanel // initializes JavaFX
    if (Platform.isFxApplicationThread) task.run() else Platform.runLater(task)
  }
}
