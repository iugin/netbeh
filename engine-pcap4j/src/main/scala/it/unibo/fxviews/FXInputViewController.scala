package it.unibo.fxviews

/**
  * A trait that gives generic methods that all JavaFX input controllers should have
  */
trait FXInputViewController {
  this: FXViewController =>

  override def showGUI(): Unit = {
    initGUI()
    resetFields()
    stage show()
  }

  /**
    * Resets input fields.
    * Called the first time after initialization of the GUI and before displaying it
    */
  def resetFields(): Unit

  /**
    * Disables View components
    */
  def disableViewComponents(): Unit

  /**
    * Enables disabled view components
    */
  def enableViewComponents(): Unit
}
