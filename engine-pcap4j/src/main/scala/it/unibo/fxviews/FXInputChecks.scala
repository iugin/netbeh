package it.unibo.fxviews

import javafx.scene.control.{CheckBox, ChoiceBox, Spinner, TextField}

/**
  * A trait describing common checks over view input
  */
trait FXInputChecks extends FXAlertsController {

  private val WRONG_INPUT_ERROR = "Wrong input!"

  /**
    * Gets a text field value
    *
    * @param field the field to get
    * @return optionally the text into field
    */
  def getTextFieldValue(field: TextField): Option[String] =
    if (field != null) Option(field.getText)
    else None

  /**
    * Gets a text field or show an error
    *
    * @param field   the field to get
    * @param message the message to show if field empty
    * @return optionally the field text
    */
  def getTextFieldValue(field: TextField, message: String): Option[String] =
    getTextFieldValue(field) match {
      case s@Some(_) => s
      case None => showError(WRONG_INPUT_ERROR, message); None
    }

  /**
    * Gets the spinner value
    *
    * @param spinner the spinner on which to work
    * @tparam A the type of objects into spinner
    * @return optionally the spinner value
    */
  def getSpinnerFieldValue[A](spinner: Spinner[A]): Option[A] =
    if (spinner != null) Option(spinner.getValue)
    else None

  /**
    * Gets spinner value or shows an error
    *
    * @param spinner the spinner on which to work
    * @param message the message to show if value not present
    * @tparam A the type of spinner values
    * @return optionally the spinner value
    */
  def getSpinnerFieldValue[A](spinner: Spinner[A], message: String): Option[A] =
    getSpinnerFieldValue(spinner) match {
      case s@Some(_) => s
      case None => showError(WRONG_INPUT_ERROR, message); None
    }

  /**
    * Gets the check box value
    *
    * @param checkBox the check box on which to work
    * @return optionally the checkbox value
    */
  def getCheckBoxValue(checkBox: CheckBox): Option[Boolean] =
    if (checkBox != null) Option(checkBox.isSelected)
    else None

  /**
    * Gets the checkbox value or show an error message
    *
    * @param checkBox the checkbox on which to work
    * @param message  the message to show on error
    * @return optionally the checkbox value
    */
  def getCheckBoxValue(checkBox: CheckBox, message: String): Option[Boolean] =
    getCheckBoxValue(checkBox) match {
      case s@Some(_) => s
      case None => showError(WRONG_INPUT_ERROR, message); None
    }

  /**
    * Gets the choicebox value
    *
    * @param choiceBox the choicebox on which to work
    * @tparam T the type of field managed by the choicebox
    * @return optionally the choicebox value selected
    */
  def getChoiceBoxValue[T](choiceBox: ChoiceBox[T]): Option[T] =
    if (choiceBox != null) Option(choiceBox.getValue)
    else None

  /**
    * Gets the choicebox value or show an error message
    *
    * @param choiceBox the choicebox on which to work
    * @param message  the message to show on error
    * @tparam T the type of field managed by the choicebox
    * @return optionally the choicebox value selected
    */
  def getChoiceBoxValue[T](choiceBox: ChoiceBox[T], message: String): Option[T] = {
    getChoiceBoxValue(choiceBox) match {
      case s@Some(_) => s
      case None => showError(WRONG_INPUT_ERROR, message); None
    }
  }
}
