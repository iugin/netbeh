package it.unibo.fxviews

import javafx.application.Platform

trait FXViewApplicationController extends FXViewController {

  /**
    * Tells which action to perform on window close
    *
    * @return the default close action (exits JVM)
    */
  protected def onCloseAction(): Unit = {
    Platform.exit()
    System.exit(0)
  }

  override def initGUI(): Unit = {
    super.initGUI()
    stage setOnCloseRequest (_ => onCloseAction())
    Platform setImplicitExit false
  }
}
