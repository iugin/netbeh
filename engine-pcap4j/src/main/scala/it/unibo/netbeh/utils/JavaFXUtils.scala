package it.unibo.netbeh.utils

import javafx.beans.property.SimpleObjectProperty
import javafx.geometry.Point2D
import javafx.scene.chart.AreaChart
import javafx.scene.control.Tooltip

object JavaFXUtils {

  /**
    * Utility method that enable the hover tooltip in the chart for each node.
    *
    * @param sc      the chart
    * @param onHover the callback that generates the string content for each node
    */
  def enableChartTooltip(sc: AreaChart[Number, Number], onHover: (AreaChart[Number, Number], Double, Double) => String = seriesValues): Unit = {
    val mouseLocationInScene = new SimpleObjectProperty[Point2D]()
    val tooltip = new Tooltip()
    tooltip.setHideOnEscape(true)
    tooltip.setAutoHide(true)
    sc.setOnMouseExited(_ => tooltip.hide())
    sc.setOnMouseMoved(evt => {
      // Compute the location
      mouseLocationInScene.set(new Point2D(evt.getSceneX, evt.getSceneY))
      val x = sc.getXAxis.getValueForDisplay(sc.getXAxis.sceneToLocal(mouseLocationInScene.get).getX).doubleValue
      val y = sc.getYAxis.getValueForDisplay(sc.getYAxis.sceneToLocal(mouseLocationInScene.get).getY).doubleValue
      // Show the popup
      onHover(sc, x, y) match {
        case s if !s.isEmpty =>
          tooltip.setText(s)
          val popupXY = sc.localToScreen(sc.sceneToLocal(mouseLocationInScene.get))
          tooltip.setX(popupXY.getX)
          tooltip.setY(popupXY.getY)
          tooltip.show(sc.getScene.getWindow)
        case _ =>
      }
    })
  }

  /*
   * Obtains each value of the series in that x position
   */
  private def seriesValues(ac: AreaChart[Number, Number], x: Double, y: Double): String = {
    var res = StringBuilder.newBuilder
    ac.getData.forEach(series =>
      try {
        val data = series.getData.stream().filter(_.getXValue.intValue == x.round.toInt).findFirst()
        if (data.isPresent) {
          res ++= s"${series.getName}: (${data.get.getXValue}, ${data.get.getYValue})\n"
        }
      } catch {
        case _: IndexOutOfBoundsException =>
      }
    )
    res.mkString.trim
  }
}
