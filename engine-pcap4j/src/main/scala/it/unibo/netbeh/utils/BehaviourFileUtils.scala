package it.unibo.netbeh.utils

import java.io.File

import it.unibo.netbeh.core.behaviour.{DeviceBehaviour, WindowBehaviour}
import it.unibo.netbeh.internet.engine.behaviours.InternetServiceBehaviour._
import it.unibo.netbeh.internet.engine.behaviours.{InternetAtomicBehaviour, InternetServiceBehaviour}
import it.unibo.utils.FileUtils._

import scala.util.Try

object BehaviourFileUtils {

  // Last window saved to file, used to fill the missing window values
  private var lastWindowId = -1

  private val csvSeparator = ","

  // Support variables for the Neural Network conversion
  private val nnMaxWindowServices = 20
  private val nnNHistory = 6
  private val fakeServiceValues = (0 until 10).map(_ => 0)  // until the number of columns of each service
  private val fakeWindowLine = (0 until nnMaxWindowServices).flatMap(_ => fakeServiceValues).mkString(csvSeparator) + csvSeparator + "\"\""

  /**
    * Deletes every file in the output directory.
    *
    * @param outputDirectory the output directory clean
    */
  def clearOutput(outputDirectory: String): Unit = {
    val output = new File(outputDirectory)
    if (output.exists && output.isDirectory)
      output.listFiles.toSeq.filter(_.isFile).foreach(_.delete)
  }

  /**
    * If the file is empty, it writes the header in it.
    *
    * @param outputFile the file
    * @param header     the header to write
    */
  private def checkAndWriteHeader(outputFile: File, header: String): Unit = {
    if (!outputFile.exists || outputFile.length == 0) {
      writeToText(outputFile, header + "\n")
    }
  }

  /**
    * Saves the device behaviour into every format to each corresponding file.
    *
    * @param outputDir the output directory
    * @param windowId  the window identifier
    * @param db        the Device Behaviour to write
    */
  def saveDeviceBehaviour(outputDir: String, windowId: Int, db: DeviceBehaviour[InternetAtomicBehaviour]): Unit = {
    // If it is the first window, it updates its value
    if (lastWindowId == -1) {
      lastWindowId = windowId
    }
    writeToJson(outputDir, windowId, db)
    writeToCsv(outputDir, windowId, db)
    writeToCsvAggregatingByService(outputDir, windowId, db)
    writeToCsvForNeuralNetwork(outputDir, windowId, db)
    //    writeToCsvFullDimensions(outputDir, windowId, db)
    lastWindowId = windowId
  }

  /*
   * Write to JSON
   */
  private def writeToJson(outputDir: String, windowId: Int, db: DeviceBehaviour[InternetAtomicBehaviour]): Unit = {
    val ofJson = new File(outputDir, s"${db.device}.json")

    def convert(serviceBehaviour: Seq[InternetServiceBehaviour]): WindowBehaviour[InternetServiceBehaviour] =
      WindowBehaviour(windowId, serviceBehaviour)

    appendToJsonAsArray(ofJson,
      convert(db.serviceBehaviours)
    )
  }

  /*
   * Write to CSV
   */
  private def writeToCsv(outputDir: String, windowId: Int, db: DeviceBehaviour[InternetAtomicBehaviour]): Unit = {
    val ofCsv = new File(outputDir, s"${db.device}.csv")
    checkAndWriteHeader(ofCsv, headerCsv)
    appendLinesToText(
      ofCsv,
      db.serviceBehaviours.flatMap(sb => {
        sb.behaviours.map(ab => {
          '"' + List(
            windowId,
            sb.serviceName,
            ab.serviceName,
            ab.serviceAddress,
            ab.timestamp.getTime,
            // Incoming
            ab.nIncomingPackets,
            ab.minIncomingLength,
            ab.avgIncomingLength,
            ab.maxIncomingLength,
            ab.totalIncomingLength,
            // Outgoing
            ab.nOutgoingPackets,
            ab.minOutgoingLength,
            ab.avgOutgoingLength,
            ab.maxOutgoingLength,
            ab.totalOutgoingLength,
            // Ports
            ab.servicePort,
            ab.devicePorts.mkString(",")
          ).mkString("\";\"") + '"'
        })
      })
    )
  }

  // The header for the complete CSV file
  private val headerCsv = '"' + List(
    "Window",
    "ServiceName",
    "ServiceName",
    "ServiceAddress",
    "Timestamp",
    // Incoming
    "nIncomingPackets",
    "minIncomingLength",
    "avgIncomingLength",
    "maxIncomingLength",
    "totalIncomingLength",
    // Outgoing
    "nOutgoingPackets",
    "minOutgoingLength",
    "avgOutgoingLength",
    "maxOutgoingLength",
    "totalOutgoingLength",
    // Ports
    "servicePorts",
    "devicePorts"
  ).mkString("\";\"") + '"'

  /*
   * Write to CSV aggregating by service
   */
  private def writeToCsvAggregatingByService(outputDir: String, windowId: Int, db: DeviceBehaviour[InternetAtomicBehaviour]): Unit = {
    val ofCsvByService = new File(outputDir, s"${db.device}-byservice.csv")
    checkAndWriteHeader(ofCsvByService, headerCsvByService)
    appendLinesToText(ofCsvByService, db.serviceBehaviours.map(sb => {
      '"' + List(
        windowId,
        sb.serviceName,
        // Incoming
        sb.nIncomingPackets,
        sb.minIncomingLength,
        sb.avgIncomingLength,
        sb.maxIncomingLength,
        sb.totalIncomingLength,
        // Outgoing
        sb.nOutgoingPackets,
        sb.minOutgoingLength,
        sb.avgOutgoingLength,
        sb.maxOutgoingLength,
        sb.totalOutgoingLength,
        sb.labels
      ).mkString("\";\"") + '"'
    }))
  }

  // The header for the CSV file aggregated by service
  private val headerCsvByService = '"' + List(
    "Window",
    "ServiceName",
    // Incoming
    "nIncomingPackets",
    "minIncomingLength",
    "avgIncomingLength",
    "maxIncomingLength",
    "totalIncomingLength",
    // Outgoing
    "nOutgoingPackets",
    "minOutgoingLength",
    "avgOutgoingLength",
    "maxOutgoingLength",
    "totalOutgoingLength",
    "labels"
  ).mkString("\";\"") + '"'

  /*
   * Writes to a file for NN elaboration
   */
  private def writeToCsvForNeuralNetwork(outputDir: String, windowId: Int, db: DeviceBehaviour[InternetAtomicBehaviour]): Unit = {
    val of = new File(outputDir, s"${db.device}-NN.csv")
    // Build the header: repeat the same header for the number of services reported
    checkAndWriteHeader(of, (0 until nnMaxWindowServices).map(headerCsvServiceForNN).mkString(csvSeparator) + csvSeparator + "\"labels\"")
    // Fill previous gaps in the sequence of windows
    // It fills at max the n of history windows that will be considered, for performance reasons
    var lines: List[String] = (1 until Math.min(nnNHistory, windowId - lastWindowId)).map(_ => fakeWindowLine).toList
    /*
     * Write current window line
     */
    // Empty lines
    lines = lines :+
    // Services in window
      (0 until nnMaxWindowServices).map(i => db.serviceBehaviours.find(_.serviceNumber == i)).flatMap {
        // In case the service is present in this window
        case Some(sb) => List(
          // Incoming
          sb.nIncomingPackets,
          sb.minIncomingLength,
          Math.round(sb.avgIncomingLength),
          sb.maxIncomingLength,
          sb.totalIncomingLength,
          // Outgoing
          sb.nOutgoingPackets,
          sb.minOutgoingLength,
          Math.round(sb.avgOutgoingLength),
          sb.maxOutgoingLength,
          sb.totalOutgoingLength
        )
        // Otherwise it fills the values with zeros
        case None => fakeServiceValues
      }.mkString(csvSeparator) +
    // Labels for window
      ",\"" + db.serviceBehaviours.flatMap(_.labels).distinct.mkString(";") + "\""
    appendLinesToText(of, lines)
  }

  // Header for the data of each service
  // (no serviceName: we know it based on the position; no windowId: line number)
  private def headerCsvServiceForNN(i: Int) = '"' + List(
    // Incoming
    "nIncomingPackets" + i,
    "minIncomingLength" + i,
    "avgIncomingLength" + i,
    "maxIncomingLength" + i,
    "totalIncomingLength" + i,
    // Outgoing
    "nOutgoingPackets" + i,
    "minOutgoingLength" + i,
    "avgOutgoingLength" + i,
    "maxOutgoingLength" + i,
    "totalOutgoingLength" + i
  ).mkString("\",\"") + '"'

  /*
   * Write to CSV
   */
  private def writeToCsvFullDimensions(outputDir: String, windowId: Int, db: DeviceBehaviour[InternetAtomicBehaviour]): Unit = {
    val of = new File(outputDir, s"${db.device}-full-dimensions.csv")
    checkAndWriteHeader(of, headerCsvFullDimensions)
    val lines = db.serviceBehaviours
      .foldLeft(Map[String, Seq[InternetServiceBehaviour]]())((r, b) => {
        val serviceName = b.serviceName.split(":")(0)
        r.get(serviceName) match {
          case Some(l) => r + (serviceName -> (b +: l))
          case None => r + (serviceName -> List(b))
        }
      })
      .map(m => {
        /*
         * Each entry is the destination host and may have multiple ports connections
         */
        // Global stats for this host
        val globalStats = m._2.foldLeft(List[Long](
          0, // Incoming Max pkt size
          0, // Incoming Min pkt size
          0, // Incoming Total length
          0, // Incoming Total n packets
          0, // Outgoing Max pkt size
          0, // Outgoing Min pkt size
          0, // Outgoing Total length
          0 // Outgoing Total n packets
        ))((r, sb) => {
          List(
            Math.max(r(0), sb.maxIncomingLength),
            Math.min(r(1), sb.minIncomingLength),
            r(2) + sb.totalIncomingLength,
            r(3) + sb.nIncomingPackets,
            Math.max(r(4), sb.maxOutgoingLength),
            Math.min(r(5), sb.minOutgoingLength),
            r(6) + sb.totalOutgoingLength,
            r(7) + sb.nOutgoingPackets
          )
        })
        // Single port data
        val services = m._2.foldLeft(Map[Int, List[Long]]())((r, sb) => r + (sb.behaviours.head.servicePort -> List(
          sb.totalIncomingLength,
          sb.nIncomingPackets,
          sb.totalOutgoingLength,
          sb.nOutgoingPackets,
        )))
        // Build the full row
        val row =
          List(
            windowId, // Window id
            m._1, // Service name
            globalStats(0), // Max incoming
            Try[Double](globalStats(2) / globalStats(3)).recover { case _: ArithmeticException => 0D }.get, // Avg incoming
            globalStats(1), // Min incoming
            globalStats(4), // Max outgoing
            Try[Double](globalStats(6) / globalStats(7)).recover { case _: ArithmeticException => 0D }.get, // Avg outgoing
            globalStats(5) // Min outgoing
          ) ++ Stream.range(1, 65535)
            .flatMap(i => services.get(i) match {
              case Some(l) => l
              case None => List.fill(4)(0L)
            })
        // Convert the row to csv
        row.mkString(";")
      })
    appendLinesToText(of, lines.toSeq)
  }

  // The header for the CSV file aggregated by service
  private val headerCsvFullDimensions = '"' + List(
    "Window",
    "ServiceName",
    "maxIncomingLength",
    "avgIncomingLength",
    "minIncomingLength",
    "maxOutgoingLength",
    "avgOutgoingLength",
    "minOutgoingLength",
    (1 to 65535).flatMap(i => List(
      i + "InTotal",
      i + "InNPackets",
      i + "OutTotal",
      i + "OutNPackets"
    )).mkString("\";\"")
  ).mkString("\";\"") + '"'
}