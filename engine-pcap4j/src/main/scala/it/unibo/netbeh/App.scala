package it.unibo.netbeh

import java.io.File
import java.util.concurrent.Executors

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import it.unibo.fxviews.FXRunOnUIThread
import it.unibo.netbeh.internet.engine.InternetServiceManager
import it.unibo.netbeh.internet.engine.behaviours.{EngineExpectedServiceBehaviour, InternetServiceWindowBehaviour}
import it.unibo.netbeh.internet.pcap4j.{Pcap4JEngine, Pcap4JSourceSniffer}
import it.unibo.netbeh.internet.view._
import it.unibo.netbeh.utils.BehaviourFileUtils
import it.unibo.utils.{Configuration, FileUtils}
import org.pcap4j.core._
import org.slf4j.LoggerFactory

import scala.concurrent.{ExecutionContext, Future}

object App {

  // Logger
  private val logger = LoggerFactory.getLogger("App")

  // ExecutionContext for parallelization
  private implicit val ec: ExecutionContext = ExecutionContext.fromExecutorService(Executors.newFixedThreadPool(4))

  private var sniffer: Pcap4JSourceSniffer = Pcap4JSourceSniffer()
  var trafficChart: Option[TrafficChartFXController] = None
  var trafficServicesChart: Option[TrafficServicesChartFXController] = None

  @throws[PcapNativeException]
  @throws[NotOpenException]
  def main(args: Array[String]): Unit = {

    val parameters = parseParameters(args)
    val mode =
      if (parameters.contains("input")) "file"
      else "live"
    val file = mode match {
      // If it's a file, it first verifies there isn't an input argument with its value, otherwise it takes the default one
      case "file" => Option(
          if (parameters.contains("input")) parameters("input").toString
          else Configuration().getString("sniffer.file.input_path")
        )
      // If it's live, it first checks if the interface number was provided as argument, otherwise returns None
      case "live" => parameters.get("device").map(_.toString + "-live")
    }
    import scala.collection.JavaConverters._
    val ips =
      if (parameters.contains("ip")) parameters("ip").toString
      else Configuration().getStringList("sniffer.local_devices").asScala.mkString(",")

    if (parameters.contains("no_gui")) {
      if ((parameters.contains("input") || parameters.contains("device")) && parameters.contains("ip"))
        extractCallback(mode, file, ips)
      else {
        print("Missing parameters!")
        System.exit(1)
      }
    } else {
      // GUI
      FXRunOnUIThread.run(() => MainMenuFXController(
        () => onClickExtract(mode, file, ips),
        () => onClickAnalyze(
          defaultExtractedBehaviour = "extractedTest.json",
          defaultExpectedBehaviour = "expectedTest.json"
        )
      ).showGUI())
    }
  }

  def parseParameters(args: Array[String]) = {
    val usage =
      """
        |Options usage: [--no-gui] [-i PATH]/[-d INTERFACE] [-ip ADDRESS]
        |
        |Batch processing: --no-gui -i PATH -ip ADDRESSES
        |Live processing:  --no-gui -d INTERFACE -ip ADDRESSES
        |
        |The addresses must be comma separated
        |""".stripMargin

    if (args.length == 0) println(usage)
    val argList = args.toList
    type OptionMap = Map[String, Any]

    @scala.annotation.tailrec
    def nextOption(map: OptionMap, list: List[String]): OptionMap = {
      list match {
        case Nil => map
        case "--no-gui" :: tail =>
          nextOption(map ++ Map("no_gui" -> Nil), tail)
        case "-i" :: value :: tail =>
          nextOption(map ++ Map("input" -> value), tail)
        case "-ip" :: value :: tail =>
          nextOption(map ++ Map("ip" -> value), tail)
        case "-d" :: value :: tail =>
          nextOption(map ++ Map("device" -> value), tail)
        case option :: tail =>
          println("Unknown option " + option)
          System.exit(1)
          map
      }
    }

    nextOption(Map(), argList)
  }

  def extractCallback(mode: String, opt_file: Option[String], deviceIPs: String)(implicit ec: ExecutionContext) = {
    val file = opt_file.getOrElse("live")
    // Open sniffer
    sniffer = mode match {
      case "live" =>
        // Take the device number, if present, and parse it to int
        sniffer.openLive(opt_file.map(_.split("-").head.toInt))
      case "file" =>
        sniffer.fromFilePcap(s"$file.pcap")
        sniffer.setLabelFile(s"$file.pcap.proto")
    }
    val ipAddressList = deviceIPs.split(",").map(_.trim).toList

    // Output preparation
    var windowId = 0
    BehaviourFileUtils.clearOutput(file)
    val unknownServicesLogFile = new File(file + ".unknown.log")
    FileUtils.cleanFile(unknownServicesLogFile)
    val outputFolder = if (file == null || file.isEmpty) "output" else file

    // Data-flow
    Pcap4JEngine(ipAddressList).start(sniffer.start(ipAddressList, 0)
      .doOnNext(l =>
        // Check termination status
        if (l == null) {
          logger.info("Program terminated")
          System.exit(0)
        })
    )
      .doOnNext(l => Future {
        logger.debug("behaviour list size: " + l.head.serviceBehaviours.size)
      })
      .subscribe(l => {
        // write to file for late analysis
        logger.debug("Writing to output...")
        logger.info("Stage 3: releasing new window")

        // Logging unknown services
        FileUtils.writeLinesToText(unknownServicesLogFile,
          InternetServiceManager.getUnknownServices().map(e => s"${e._1} -> ${e._2}").toList)

        // Save each device behaviour to file
        l.foreach(db => BehaviourFileUtils.saveDeviceBehaviour(outputFolder, windowId, db))

        // Increment the window counter
        windowId += 1
      })
  }

  def analyzeCallback(extracted: String, expected: String) = {
    if (trafficChart.isEmpty || trafficServicesChart.isEmpty) {
      FXRunOnUIThread.run(() => {
        trafficChart = Option(TrafficChartFXController())
        trafficServicesChart = Option(TrafficServicesChartFXController())
      })
    }
    val mapper = new ObjectMapper()
    mapper.registerModule(DefaultScalaModule)

    val expectedServiceBehaviour = mapper.readValue(
      org.apache.commons.io.FileUtils.readFileToString(new File(expected), "UTF-8"),
      classOf[EngineExpectedServiceBehaviour]
    )

    val extractedDeviceBehaivour = mapper.readValue(
      org.apache.commons.io.FileUtils.readFileToString(new File(extracted), "UTF-8"),
      classOf[Array[InternetServiceWindowBehaviour]]
    )
    val extractedServiceBehaviour = extractedDeviceBehaivour
      .flatMap(i => i.behaviourList.filter(_.serviceName == expectedServiceBehaviour.serviceName))

    val result = expectedServiceBehaviour.validate(extractedServiceBehaviour)

    FXRunOnUIThread.run(() => {
      trafficChart.get.showGUI()
      trafficServicesChart.get.showGUI()
    })
    println("Risultato: " + result)

    // Updates the GUI data
    extractedDeviceBehaivour.takeRight(Configuration().getInt("gui.chart.history")).foreach(b => {
      trafficChart.get.update(b.windowId, b.behaviourList)
      trafficServicesChart.get.update(b.windowId, b.behaviourList)
    })
  }

  private def onClickExtract(defaultMode: String, defaultFilePath: Option[String], defaultDeviceIPs: String): Unit = {
    val gui = ExtractionFXController(
      defaultMode,
      defaultFilePath,
      defaultDeviceIPs,
      startCallback = extractCallback,
      stopCallback = () => sniffer.stop()
    )
    FXRunOnUIThread.run(() => gui.showGUI())
  }

  private def onClickAnalyze(defaultExtractedBehaviour: String, defaultExpectedBehaviour: String)(implicit ec: ExecutionContext): Unit = {
    val gui = AnalysisFXController(
      defaultExtractedBehaviour,
      defaultExpectedBehaviour,
      analyzeCallback = analyzeCallback
    )
    FXRunOnUIThread.run(() => gui.showGUI())
  }

}
