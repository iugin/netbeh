package it.unibo.netbeh.internet.view

import it.unibo.fxviews.{FXInputChecks, FXInputViewController, FXViewController}
import it.unibo.netbeh.internet.engine.behaviours.InternetServiceBehaviour
import it.unibo.netbeh.utils.JavaFXUtils
import javafx.fxml.FXML
import javafx.scene.chart.{AreaChart, XYChart}
import javafx.scene.control.Slider
import javafx.scene.layout.{FlowPane, HBox}

/**
  * GUI layout in javaFX for the chart representation of the traffic
  *
  */
case class TrafficServicesChartFXController(
                               ) extends FXViewController with FXChartController with FXInputViewController with FXInputChecks {

  @FXML private var chartBox: FlowPane = _
  @FXML private var scaleSlider: Slider = _

  override protected val layout: String = "/layouts/trafficServicesChartLayout.fxml"

  override protected val title: String = "NetBeh - traffic graph"

  override protected val controller: FXViewController = this

  private val SERIES_TOTAL: String = "Total (MB)"
  private val SERIES_PACKETS: String = "N. Packets"
  private val SERIES_MIN: String = "Min pkt size"
  private val SERIES_AVG: String = "Avg pkt size"
  private val SERIES_MAX: String = "Max pkt size"

  private var lastWindowId = 0

  override def resetFields(): Unit = {}

  override def disableViewComponents(): Unit = {}

  override def enableViewComponents(): Unit = {}

  override def initGUI(): Unit = {
    super.initGUI()
    scaleSlider.valueProperty.addListener((observable, oldValue, newValue) => {
      updateChartScale(newValue.doubleValue)
    })
  }

  def update(window: Int, behaviours: Seq[InternetServiceBehaviour]): Unit = runOnUIThread(() => {
    // If there is a gap in the data, add zero values until its filled
    while (lastWindowId < window-1) {
      lastWindowId = if (lastWindowId == 0) window else lastWindowId + 1
      chartMap.values.foreach(addZerosToChart(lastWindowId, _))
    }
    // Then add the actual value
    behaviours.foreach((service: InternetServiceBehaviour) => {
      if (!chartMap.contains(chartTitle(service.serviceName, incoming = true))) {
        // If its a new service, creates the chart for both incoming and outgoing traffic
        val incomingChart = buildChart(chartTitle(service.serviceName, incoming = true))
        val outgoingChart = buildChart(chartTitle(service.serviceName, incoming = false))
        incomingChart.foreach(c => {
          chartMap += (c.getTitle -> c)
        })
        outgoingChart.foreach(c => {
          chartMap += (c.getTitle -> c)
        })
        // display chart in view
        chartBox.getChildren.add(new HBox(incomingChart ++ outgoingChart :_*))
      }
      updateChart(window, service)
    })
    // Final operations
    chartMap.values.foreach(ac => {
      // If any series is missing the value for the current windowId, then its added a zero value in place of it
      addZerosToChart(window, ac)
      // Then update the chart visualization
      updateChartVisualization(window, ac)
    })
    lastWindowId += 1
  })

  /**
    * Returns the graph name for the service name passed
    * @param name the name of the service
    * @param incoming if its regarding the incoming or outgoing traffic
    * @return
    */
  private def chartTitle(name: String, incoming: Boolean): String = if (incoming) {
    name + " - Incoming"
  } else {
    name + " - Outgoing"
  }

  /**
    * Builds a chart and returns it.
    * It also initializes all the series and its legends.
    * @param name the name of the chart
    * @return the chart itself
    */
  private def buildChart(name: String): List[AreaChart[Number, Number]] = {
    // Create the first graph
    val acTotal = newChart(name + " total")
    // Create the second graph
    val acPackets = newChart(name + " packets")
    // Create the third graph
    val acStat = newChart(name)
    // Prepare the axis
    val seriesTotal = new XYChart.Series[Number, Number]()
    seriesTotal.setName(SERIES_TOTAL)
    val seriesPackets = new XYChart.Series[Number, Number]()
    seriesPackets.setName(SERIES_PACKETS)
    val seriesMin = new XYChart.Series[Number, Number]()
    seriesMin.setName(SERIES_MIN)
    val seriesAvg = new XYChart.Series[Number, Number]()
    seriesAvg.setName(SERIES_AVG)
    val seriesMax = new XYChart.Series[Number, Number]()
    seriesMax.setName(SERIES_MAX)
    // Insert the series
    acTotal.getData.addAll(seriesTotal)
    acPackets.getData.addAll(seriesPackets)
    acStat.getData.addAll(seriesMin, seriesAvg, seriesMax)
    // Enable the tooltips
    JavaFXUtils.enableChartTooltip(acTotal)
    JavaFXUtils.enableChartTooltip(acPackets)
    JavaFXUtils.enableChartTooltip(acStat)

    val result = List(acTotal, acPackets, acStat)
    result.foreach(chart => {
      JavaFXUtils.enableChartTooltip(chart)
    })
    result
  }

  /**
    * Updates the chart live adding its new window item
    * @param window the number of the window
    * @param behaviour the service behaviour to update
    */
  private def updateChart(window: Int, behaviour: InternetServiceBehaviour): Unit = {
    val acIncomingTot = chartMap.get(chartTitle(behaviour.serviceName, incoming = true) + " total")
    val acIncomingPkt = chartMap.get(chartTitle(behaviour.serviceName, incoming = true) + " packets")
    val acIncomingStat = chartMap.get(chartTitle(behaviour.serviceName, incoming = true))
    val acOutgoingTot = chartMap.get(chartTitle(behaviour.serviceName, incoming = false) + " total")
    val acOutgoingPkt = chartMap.get(chartTitle(behaviour.serviceName, incoming = false) + " packets")
    val acOutgoingStat = chartMap.get(chartTitle(behaviour.serviceName, incoming = false))

    /**
      * Updates the incoming areachart utilizing the incoming parameters
      * @param ac the areaChart
      */
    def updateIncoming(ac: AreaChart[Number, Number]): Unit = {
      ac.getData.forEach((series: XYChart.Series[Number, Number]) => {
        // Add last data
        series.getName match {
          case SERIES_TOTAL => series.getData.add(new XYChart.Data(window,behaviour.totalIncomingLength.toDouble/1000000))
          case SERIES_PACKETS => series.getData.add(new XYChart.Data(window,behaviour.nIncomingPackets))
          case SERIES_MIN => series.getData.add(new XYChart.Data(window,behaviour.minIncomingLength))
          case SERIES_AVG => series.getData.add(new XYChart.Data(window,behaviour.avgIncomingLength))
          case SERIES_MAX => series.getData.add(new XYChart.Data(window,behaviour.maxIncomingLength))
        }
      })
      updateChartVisualization(window, ac)
    }
    /**
      * Updates the incoming areachart utilizing the outgoing parameters
      * @param ac the areaChart
      */
    def updateOutgoing(ac: AreaChart[Number, Number]): Unit = {
      ac.getData.forEach((series: XYChart.Series[Number, Number]) => {
        // Add last data
        series.getName match {
          case SERIES_TOTAL => series.getData.add(new XYChart.Data(window,behaviour.totalOutgoingLength.toDouble/1000000))
          case SERIES_PACKETS => series.getData.add(new XYChart.Data(window,behaviour.nOutgoingPackets))
          case SERIES_MIN => series.getData.add(new XYChart.Data(window,behaviour.minOutgoingLength))
          case SERIES_AVG => series.getData.add(new XYChart.Data(window,behaviour.avgOutgoingLength))
          case SERIES_MAX => series.getData.add(new XYChart.Data(window,behaviour.maxOutgoingLength))
        }
      })
      updateChartVisualization(window, ac)
    }

    // Incoming graph
    acIncomingTot.foreach(updateIncoming)
    acIncomingPkt.foreach(updateIncoming)
    acIncomingStat.foreach(updateIncoming)

    // Outgoing graph
    acOutgoingTot.foreach(updateOutgoing)
    acOutgoingPkt.foreach(updateOutgoing)
    acOutgoingStat.foreach(updateOutgoing)
  }
}