package it.unibo.netbeh.internet.pcap4j.properties

import it.unibo.netbeh.internet.engine.properties.InternetProperties
import org.pcap4j.packet.Packet

abstract class Pcap4jPropertiesPacket(protected val packet: Packet) extends InternetProperties {

}
