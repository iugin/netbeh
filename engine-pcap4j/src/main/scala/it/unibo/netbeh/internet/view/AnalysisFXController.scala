package it.unibo.netbeh.internet.view

import it.unibo.fxviews.{FXInputChecks, FXInputViewController, FXViewController}
import javafx.fxml.FXML
import javafx.scene.control.TextField

import scala.concurrent.{ExecutionContext, Future}

/**
  * GUI layout in javaFX for the main analysis application panel
  *
  * @param analyzeCallback start the analysis of the behaviour
  */
case class AnalysisFXController(
                                 defaultExtractedBehaviour: String,
                                 defaultExpectedBehaviour: String,
                                 analyzeCallback: (String, String) => Unit
                               )(implicit ec: ExecutionContext) extends FXViewController with FXInputViewController with FXInputChecks {

  @FXML private var inExtractedBehaviourFile: TextField = _
  @FXML private var inExpectedBehaviourFile: TextField = _

  override protected def layout: String = "/layouts/analysisLayout.fxml"

  override protected def title: String = "NetBeh"

  override protected def controller: FXViewController = this

  override def resetFields(): Unit = {
    inExtractedBehaviourFile.setText(defaultExtractedBehaviour)
    inExpectedBehaviourFile.setText(defaultExpectedBehaviour)
  }

  override def disableViewComponents(): Unit = {}

  override def enableViewComponents(): Unit = {}

  @FXML private def onClickAnalyze(): Unit =
    Future{
      for (
        extractedBehaviour <- getTextFieldValue(inExtractedBehaviourFile, "Invalid extracted behaviour file");
        expectedBehaviour <- getTextFieldValue(inExpectedBehaviourFile, "Invalid expected behaviour file")
      ) yield {
        analyzeCallback(extractedBehaviour, expectedBehaviour)
        runOnUIThread(() => showInfo("Files di analisi", s"$extractedBehaviour =?= $expectedBehaviour"))
      }
    }

}