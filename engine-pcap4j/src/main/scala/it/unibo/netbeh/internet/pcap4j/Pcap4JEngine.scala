package it.unibo.netbeh.internet.pcap4j

import java.sql.Timestamp

import it.unibo.netbeh.internet.engine.InternetEngine
import it.unibo.netbeh.internet.engine.modules.Stage1
import org.pcap4j.packet.Packet

case class Pcap4JEngine(localAddresses: Seq[String])
  extends InternetEngine[Pcap4JProperties, (Timestamp, (Packet, String))](localAddresses) {

  override protected def stage1(): Stage1[(Timestamp, (Packet, String)), Pcap4JProperties] =
    Stage1[(Timestamp, (Packet, String)), Pcap4JProperties](
      o => Pcap4JProperties(packet = o._2._1, timestamp = o._1, label = o._2._2)
    )
}
