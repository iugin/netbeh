package it.unibo.netbeh.internet.engine.behaviours

import java.sql.Timestamp

import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.fasterxml.jackson.databind.node.ArrayNode
import com.fasterxml.jackson.databind.{DeserializationContext, JsonDeserializer, ObjectMapper}
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import it.unibo.netbeh.core.behaviour
import it.unibo.netbeh.core.behaviour.AtomicBehaviour
import it.unibo.netbeh.internet.engine.InternetServiceManager
import it.unibo.netbeh.internet.engine.properties.InternetProperties

/**
  * An implementation of the AtomicBehaviour with some extensions.
  * This is the object used in the PoC.
  */
@JsonDeserialize(using = classOf[InternetAtomicBehaviour.EngineAtomicBehaviourDeserializer])
trait InternetAtomicBehaviour extends AtomicBehaviour
  with behaviour.extensions.DeviceExtension
  with behaviour.extensions.TimeExtension
  with behaviour.extensions.CounterExtension
  with behaviour.extensions.ConnectionsExtension
  with behaviour.extensions.NeuralNetworkExtension {

  @JsonProperty override def avgIncomingLength: Double = super.avgIncomingLength
  @JsonProperty override def avgOutgoingLength: Double = super.avgOutgoingLength
  @JsonProperty override def serviceNumber: Int = InternetServiceManager.serviceNumber(this)

  // For neural network
  def labels: Seq[String]

  /**
    * Merge 2 Behaviours into one and returns it
    * @param behaviour the second behaviour
    * @return the new Behaviour containing the merged values
    */
  override def +[A <: AtomicBehaviour](behaviour: A): InternetAtomicBehaviour
}

object InternetAtomicBehaviour {

  /**
    * Creates an AtomicBehaviour extracting the values from the single packet given.
    * @param p the packet
    * @param outgoing true if the packet belongs to the outgoing flow, false otherwise
    * @return the AtomicBehaviour corresponding
    */
  def apply[T<:InternetProperties](p: T, outgoing: Boolean): InternetAtomicBehaviour = if (outgoing) {
    InternetAtomicBehaviourImpl(
      p.sourceHostAddress,
      p.destinationHostName,
      s"${p.destinationHostAddress}:${p.destinationPort}",
      p.timestamp,
      minOutgoingLength = p.length,
      maxOutgoingLength = p.length,
      totalOutgoingLength = p.length,
      nOutgoingPackets = 1,
      servicePort = p.destinationPort,
      devicePorts = List(p.sourcePort),
      labels = List(p.label)
    )
  } else {
    InternetAtomicBehaviourImpl(
      p.destinationHostAddress,
      p.sourceHostName,
      s"${p.sourceHostAddress}:${p.sourcePort}",
      p.timestamp,
      minIncomingLength = p.length,
      maxIncomingLength = p.length,
      totalIncomingLength = p.length,
      nIncomingPackets = 1,
      servicePort = p.sourcePort,
      devicePorts = List(p.destinationPort),
      labels = List(p.label)
    )
  }

  case class InternetAtomicBehaviourImpl(
                                        device: String,
                                        serviceName: String,
                                        serviceAddress: String,
                                        timestamp: Timestamp,
                                        var minIncomingLength: Int = 0,
                                        var maxIncomingLength: Int = 0,
                                        var totalIncomingLength: Long = 0,
                                        var minOutgoingLength: Int = 0,
                                        var maxOutgoingLength: Int = 0,
                                        var totalOutgoingLength: Long = 0,
                                        var nIncomingPackets: Long = 0,
                                        var nOutgoingPackets: Long = 0,
                                        var servicePort: Int,
                                        var devicePorts: Seq[Int] = List(),
                                        var labels: Seq[String] = List()
                                      ) extends InternetAtomicBehaviour {

    override def +[A <: AtomicBehaviour](o: A): InternetAtomicBehaviour = {
      if (!o.isInstanceOf[InternetAtomicBehaviour]) throw new IllegalArgumentException("The argument must be an EngineAtomicBehaviour")
      val o2 = o.asInstanceOf[InternetAtomicBehaviour]
      this.minIncomingLength = (this.minIncomingLength, o2.minIncomingLength) match{
        case (0, b) => b
        case (a, 0) => a
        case (a, b) => Math.min(a, b)
      }
      this.maxIncomingLength = Math.max(this.maxIncomingLength, o2.maxIncomingLength)
      this.totalIncomingLength = this.totalIncomingLength + o2.totalIncomingLength
      this.minOutgoingLength = (this.minOutgoingLength, o2.minOutgoingLength) match{
        case (0, b) => b
        case (a, 0) => a
        case (a, b) => Math.min(a, b)
      }
      this.maxOutgoingLength = Math.max(this.maxOutgoingLength, o2.maxOutgoingLength)
      this.totalOutgoingLength = this.totalOutgoingLength + o2.totalOutgoingLength
      this.nIncomingPackets = this.nIncomingPackets + o2.nIncomingPackets
      this.nOutgoingPackets = this.nOutgoingPackets + o2.nOutgoingPackets
      this.devicePorts = (this.devicePorts ++ o2.devicePorts).distinct
      // Labels for neural network
      this.labels = (this.labels ++ o2.labels).distinct
      this
    }

    override def equals(obj: Any): Boolean = obj.isInstanceOf[InternetAtomicBehaviour] &&
      obj.asInstanceOf[InternetAtomicBehaviour].serviceName == this.serviceName &&
      obj.asInstanceOf[InternetAtomicBehaviour].serviceAddress == this.serviceAddress &&
      obj.asInstanceOf[InternetAtomicBehaviour].servicePort == this.servicePort
  }

  /**
    * Default jackson deserializer
    */
  class EngineAtomicBehaviourDeserializer extends JsonDeserializer[InternetAtomicBehaviour] {

    override def deserialize(p: JsonParser, ctxt: DeserializationContext): InternetAtomicBehaviour = {
      import com.fasterxml.jackson.databind.JsonNode
      val node: JsonNode = p.getCodec.readTree(p)

      import scala.collection.JavaConverters._
      val mapper = new ObjectMapper()
      mapper.registerModule(DefaultScalaModule)

      // Recreates the object by deserializing the unit objects one by one
      InternetAtomicBehaviourImpl(
        node.get("device").asText(),
        node.get("serviceName").asText(),
        node.get("serviceAddress").asText(),
        new Timestamp(node.get("timestamp").asLong()),
        node.get("minIncomingLength").asInt(),
        node.get("maxIncomingLength").asInt(),
        node.get("totalIncomingLength").asLong(),
        node.get("minOutgoingLength").asInt(),
        node.get("maxOutgoingLength").asInt(),
        node.get("totalOutgoingLength").asLong(),
        node.get("nIncomingPackets").asLong(),
        node.get("nOutgoingPackets").asLong(),
        node.get("servicePort").asInt(),
        node.get("devicePorts").asInstanceOf[ArrayNode].elements().asScala.map(_.asInt).toSeq,
        node.get("labels").asInstanceOf[ArrayNode].elements().asScala.map(_.asText).toSeq
      )
    }
  }
}
