package it.unibo.netbeh.internet.engine.modules

import it.unibo.netbeh.core.behaviour.extensions.{DeviceExtension, TimeExtension}
import it.unibo.netbeh.core.behaviour.{AtomicBehaviour, DeviceBehaviour}
import rx.lang.scala.Observable

import scala.concurrent.duration.Duration

case class Stage3[
  I <: AtomicBehaviour with TimeExtension with DeviceExtension,
  O <: DeviceBehaviour[I]
](
   windowSize: Duration,
   builder: String => O
 ) extends it.unibo.netbeh.core.engine.Stage3[I, O] {

  override def run(input: Observable[I]): Observable[Seq[O]] = Observable[Seq[O]] { observer =>
    // Map containing all the Atomic Behaviour for each device (the key)
    var behaviourDB: Seq[O] = List()
    var lastWindow: Long = 0
    input.subscribe(b => {
      // Eventually restart the analysis window
      // And fill window gaps (when no data is exchanged it will return an empty Device Behaviour)
      while (b.timestamp.getTime - lastWindow >= windowSize.toMillis) {
        if (lastWindow == 0) {
          lastWindow = b.timestamp.getTime
        } else {
          // Return the new list
          observer.onNext(behaviourDB)
          // Reset the old list variable
          behaviourDB = List()
          // Update window time (by a fixed amount of millis)
          lastWindow += windowSize.toMillis
        }
      }
      // If there isn't an entry for this device create one for it
      if (behaviourDB.count(_.device == b.device) == 0) behaviourDB = builder(b.device) +: behaviourDB
      // Update the atomic behaviour already present or add a new one if not
      behaviourDB.filter(_.device == b.device).foreach(_ merge b)
    })
  }
}
