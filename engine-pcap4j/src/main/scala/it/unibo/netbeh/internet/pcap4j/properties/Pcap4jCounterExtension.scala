package it.unibo.netbeh.internet.pcap4j.properties

import it.unibo.netbeh.core.properties.extensions.CounterExtension

trait Pcap4jCounterExtension extends Pcap4jPropertiesPacket with CounterExtension {

  override var length: Int = 0

  override def extractProperties: Boolean =
    super.extractProperties && (for (
      // Check packet not null
      ethPkt <- Option(packet)
    ) yield {
      length = ethPkt.length()
    }).isDefined
}
