package it.unibo.netbeh.internet.engine.modules

import it.unibo.netbeh.core.behaviour.AtomicBehaviour
import it.unibo.netbeh.core.properties.PropertiesPacket
import it.unibo.netbeh.core.properties.extensions.{AddressExtension, TimeExtension}
import rx.lang.scala.Observable

case class Stage2[
  I <: PropertiesPacket with TimeExtension with AddressExtension,
  O <: AtomicBehaviour
](
   localAddresses: Seq[String],
   builder: (I, Boolean) => O
 ) extends it.unibo.netbeh.core.engine.Stage2[I, O] {

  override def run(input: Observable[I]): Observable[O] = Observable[O] { observer =>
    input.subscribe(p => {
      // If the packet doesn't contains any interesting address, it is dropped.
      if ((localAddresses contains p.sourceHostAddress) || (localAddresses contains p.destinationHostAddress))
      // It recognizes the flow direction based on the position of the address we want to watch.
        observer.onNext(builder(p, localAddresses contains p.sourceHostAddress))
    })
  }
}
