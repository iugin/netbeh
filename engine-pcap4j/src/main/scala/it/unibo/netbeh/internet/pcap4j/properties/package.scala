package it.unibo.netbeh.internet.pcap4j

import scala.reflect.{ClassTag, classTag}

package object properties {

  /**
    * Implicitly adds some functions to the pcap4j packet
    * @param packet the packet implicitly decorated
    */
  implicit class RichPacket(packet: org.pcap4j.packet.Packet) {

    /**
      * Returns an instance of that packet if it contains its type
      * @tparam T the type of the class
      * @return an optional containing the typed packet, if present
      */
    def as[T<:org.pcap4j.packet.Packet:ClassTag]: Option[T] =
      Option(packet.get(classTag[T].runtimeClass.asInstanceOf[Class[T]]))

  }
}
