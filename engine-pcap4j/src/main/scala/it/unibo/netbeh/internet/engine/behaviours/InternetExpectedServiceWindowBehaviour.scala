package it.unibo.netbeh.internet.engine.behaviours

import it.unibo.netbeh.core.behaviour.ExpectedBehaviour

/**
  * This class represents the expected service behaviour:
  * it validates every single ServiceBehaviour given.
  * It is used to validate the behaviour recognized in a single window.
  */
case class InternetExpectedServiceWindowBehaviour(
                                           serviceName: String,
                                           // Incoming packets counters
                                           minIncomingLengthExp: Int,
                                           minIncomingLengthExpVariance: Int,
                                           maxIncomingLengthExp: Int,
                                           maxIncomingLengthExpVariance: Int,
                                           avgIncomingLengthExp: Int,
                                           avgIncomingLengthExpVariance: Int,
                                           totalIncomingLengthExp: Int,
                                           totalIncomingLengthExpVariance: Int,
                                           nIncomingPacketsExp: Int,
                                           nIncomingPacketsExpVariance: Int,
                                           // Outgoing packets counters
                                           minOutgoingLengthExp: Int,
                                           minOutgoingLengthExpVariance: Int,
                                           maxOutgoingLengthExp: Int,
                                           maxOutgoingLengthExpVariance: Int,
                                           avgOutgoingLengthExp: Int,
                                           avgOutgoingLengthExpVariance: Int,
                                           totalOutgoingLengthExp: Int,
                                           totalOutgoingLengthExpVariance: Int,
                                           nOutgoingPacketsExp: Int,
                                           nOutgoingPacketsExpVariance: Int
                                         ) extends ExpectedBehaviour[InternetServiceBehaviour] {
  override def isRespectedBy(b: InternetServiceBehaviour): Boolean =
  // Check incoming packets
    isBetweenWithVariance(b.minIncomingLength, minIncomingLengthExp, minIncomingLengthExpVariance) &&
      isBetweenWithVariance(b.maxIncomingLength, maxIncomingLengthExp, maxIncomingLengthExpVariance) &&
      isBetweenWithVariance(b.avgIncomingLength, avgIncomingLengthExp, avgIncomingLengthExpVariance) &&
      isBetweenWithVariance(b.totalIncomingLength, totalIncomingLengthExp, totalIncomingLengthExpVariance) &&
      isBetweenWithVariance(b.nIncomingPackets, nIncomingPacketsExp, nIncomingPacketsExpVariance) &&
      // Check outgoing packets
      isBetweenWithVariance(b.minOutgoingLength, minOutgoingLengthExp, minOutgoingLengthExpVariance) &&
      isBetweenWithVariance(b.maxOutgoingLength, maxOutgoingLengthExp, maxOutgoingLengthExpVariance) &&
      isBetweenWithVariance(b.avgOutgoingLength, avgOutgoingLengthExp, avgOutgoingLengthExpVariance) &&
      isBetweenWithVariance(b.totalOutgoingLength, totalOutgoingLengthExp, totalOutgoingLengthExpVariance) &&
      isBetweenWithVariance(b.nOutgoingPackets, nOutgoingPacketsExp, nOutgoingPacketsExpVariance)

  private def isBetweenWithVariance(n: Int, avgExp: Int, varianceExp: Int) = (avgExp - varianceExp) <= n && n <= (avgExp + varianceExp)

  private def isBetweenWithVariance(n: Double, avgExp: Double, varianceExp: Double) = (avgExp - varianceExp) <= n && n <= (avgExp + varianceExp)
}
