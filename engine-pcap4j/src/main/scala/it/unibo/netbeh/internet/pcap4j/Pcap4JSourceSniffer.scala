package it.unibo.netbeh.internet.pcap4j

import java.io.File
import java.sql.Timestamp

import com.sun.jna.Platform
import org.pcap4j.core.PcapHandle.TimestampPrecision
import org.pcap4j.core._
import org.pcap4j.packet.{IpV4Packet, Packet}
import org.pcap4j.util.NifSelector
import org.slf4j.LoggerFactory
import rx.lang.scala.{Observable, Subscription}

import scala.concurrent.{ExecutionContext, Future}
import scala.io.{BufferedSource, Source}

case class Pcap4JSourceSniffer()(implicit executionContext: ExecutionContext) {

  private val logger = LoggerFactory.getLogger(classOf[Pcap4JSourceSniffer])
  private var handle: PcapHandle = _
  private var dumper: PcapDumper = _
  private var dumpPath: String = ""
  private var labelPath: String = ""
  private var packetLabelsSource: BufferedSource = _

  /**
    * Open a live handle and uses it to generate the stream of packets.
    * You can only use either it or fromFilePcap
    */
  def openLive(interfaceId: Option[Int]): Pcap4JSourceSniffer = {
    val device = getNetworkDevice(interfaceId)
    logger.info("You chose: " + device)
    // New code below here
    if (device == null) {
      logger.info("No device chosen.")
      System.exit(1)
    }
    // Open the device and get a handle
    val snapshotLength = 65536 // in bytes
    val readTimeout = 50 // in milliseconds
    handle = device.openLive(snapshotLength, PcapNetworkInterface.PromiscuousMode.PROMISCUOUS, readTimeout)
    this
  }

  /**
    * Reads the dump generated from a pcap file.
    * You can only use either it or openLive
    *
    * @param path the path to the file
    */
  def fromFilePcap(path: String): Pcap4JSourceSniffer = {
    try {
      handle = Pcaps.openOffline(path, TimestampPrecision.NANO)
    } catch {
      case _: PcapNativeException => handle = Pcaps.openOffline(path)
    }
    this
  }

  /**
    * Setup the dumping.
    * It will create the dump file and write in it every packet analyzed.
    *
    * @param path the path to the dump file
    */
  def setDump(path: String): Pcap4JSourceSniffer = {
    this.dumpPath = path
    this
  }

  /**
    * Specifies the path to the label file.
    *
    * @param path the path to the label file
    */
  def setLabelFile(path: String): Pcap4JSourceSniffer = {
    this.labelPath = path
    this
  }

  private def getNetworkDevice(interfaceId: Option[Int] = None): PcapNetworkInterface = interfaceId match {
    case Some(i) =>
      import org.pcap4j.core.Pcaps
      try Pcaps.findAllDevs.get(i)
      catch {
        case _: Throwable =>
          getNetworkDevice()
      }
    case None =>
      new NifSelector().selectNetworkInterface
  }

  @throws[PcapNativeException]
  @throws[NotOpenException]
  private def printHandleStats(handle: PcapHandle): Unit = {
    try {
      val stats = handle.getStats
      logger.info("Packets received: " + stats.getNumPacketsReceived)
      logger.info("Packets dropped: " + stats.getNumPacketsDropped)
      logger.info("Packets dropped by interface: " + stats.getNumPacketsDroppedByIf)
      // Supported by WinPcap only
      if (Platform.isWindows) logger.info("Packets captured: " + stats.getNumPacketsCaptured)
    } catch {
      case e: Throwable =>
    }
  }

  private def getLabelIterator(): Iterator[List[String]] = {
    if (labelPath.isEmpty || !new File(labelPath).exists()) {
      logger.info("Missing label file")
      Iterator.empty
    } else {
      packetLabelsSource = Source.fromFile(labelPath)
      //      packetLabelsSource.getLines()
      packetLabelsSource.getLines().map(_.split(":").toList)
    }
  }

  def start(ipAddresses: List[String], maxPackets: Int = -1 /*default: unlimited*/) = Observable[(Timestamp, (Packet, String))] { observer =>
    if (!dumpPath.isEmpty) {
      dumper = handle.dumpOpen(dumpPath)
    }
    Future {
      val logger = LoggerFactory.getLogger(classOf[Pcap4JSourceSniffer])
      val packetLabels = getLabelIterator()
      try {
        val filter = s"(tcp or udp) and (src host ${ipAddresses.mkString(" or src host ")} or dst host ${ipAddresses.mkString(" or dst host ")})"
        logger.info("Handle Filter: " + filter)
        handle.setFilter(filter, BpfProgram.BpfCompileMode.OPTIMIZE)
        logger.debug("SNIFFER STARTED!")
        handle.loop(maxPackets, new PacketListener {
          override def gotPacket(packet: Packet): Unit = {
            // 1 - dump to file
            // Write packets as needed
            try {
              dumper.dump(packet, handle.getTimestamp)
            } catch {
              case _: NullPointerException =>
              case _: NotOpenException => // Nothing to do: does not dump
            }
            // 2 - Produce output
            if (packet.contains(classOf[IpV4Packet])) observer.onNext((handle.getTimestamp, (packet, getLabel(handle.getTimestamp))))

            /**
              * Get the packet label
              *
              * @param time the time the label must have
              * @return
              */
            @scala.annotation.tailrec
            def getLabel(time: Timestamp): String = {
              if (labelPath.isEmpty) ""
              else if (packetLabels.hasNext) packetLabels.next() match {
                case timestamp :: label_name :: label :: Nil if timestamp == time.getTime.toString =>
                  label
                case timestamp :: _ if timestamp.toLong < time.getTime =>
                  getLabel(time)
                case _ => throw new Exception("De-synced label")
              } else throw new Exception("De-synced label")
            }
          }
        })
      } catch {
        case _: InterruptedException => // Nothing to do: loop broken by stop
      } finally {
        logger.debug("SNIFFER FINISHED!")
        printHandleStats(handle)
        // Cleanup when complete
        packetLabelsSource.close()
        handle.close()
        Option(dumper).foreach(_.close())
        observer.onNext(null)
      }
    }
    Subscription()
  }

  def stop(): Unit = {
    Option(handle).foreach(_.breakLoop())
    Option(dumper).foreach(_.close())
  }
}
