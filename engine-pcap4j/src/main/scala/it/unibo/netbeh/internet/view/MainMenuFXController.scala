package it.unibo.netbeh.internet.view

import it.unibo.fxviews.{FXInputChecks, FXInputViewController, FXViewApplicationController, FXViewController}
import javafx.fxml.FXML

/**
  * GUI layout in javaFX for the main menu panel
  *
  */
case class MainMenuFXController(
                                 onClickExtract: () => Unit,
                                 onClickAnalyze: () => Unit
                               ) extends FXViewApplicationController with FXInputViewController with FXInputChecks {

  override protected def layout: String = "/layouts/mainMenuLayout.fxml"

  override protected def title: String = "NetBeh - main menu"

  override protected def controller: FXViewController = this

  override def resetFields(): Unit = {}

  override def disableViewComponents(): Unit = {}

  override def enableViewComponents(): Unit = {}

  @FXML private def onClickExtractG(): Unit = onClickExtract()
  @FXML private def onClickAnalyzeG(): Unit = onClickAnalyze()
}