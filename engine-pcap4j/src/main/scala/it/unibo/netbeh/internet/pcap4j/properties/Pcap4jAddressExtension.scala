package it.unibo.netbeh.internet.pcap4j.properties

import java.net.InetAddress

import it.unibo.netbeh.core.properties.extensions.AddressExtension
import it.unibo.utils.SystemUtils
import org.pcap4j.packet.{IpPacket, TcpPacket, UdpPacket}

object Pcap4jAddressExtension {

  /**
    * Cache of the rDNS request in order to improve overall efficiency in the address resolution.
    */
  private var cache: Map[String, String] = Map()
  def getCanonicalHostName(address: InetAddress): String = cache.get(address.getHostAddress) match {
    case Some(h) => h
    case None =>
      var h = address.getCanonicalHostName
      if (h == address.getHostAddress && !address.isSiteLocalAddress && !h.contains("255")) {
        import java.util.regex.Pattern

        val whois = SystemUtils.returnCommandResult(s"whois $h")

        // $NetworkNameOrIP[$country]
        searchPattern(whois, Pattern.compile("Country:\\s(.*)", Pattern.CASE_INSENSITIVE))
          .map(country => {
            searchPattern(whois, Pattern.compile("NetName:\\s(.*)", Pattern.CASE_INSENSITIVE))
              .getOrElse(h) + "[" + country + "]"
          })
          .foreach(h = _)

        def searchPattern(content: String, pattern: Pattern): Option[String] = {
          val m = pattern.matcher(content)
          if (m.find) Some(m.group(1).trim)
          else None
        }
      }
      cache = cache + (address.getHostAddress -> h)
      h
  }
}

trait Pcap4jAddressExtension extends Pcap4jPropertiesPacket with AddressExtension {

  override var sourceHostAddress: String = _
  override var sourceHostName: String = _
  override var sourcePort: Int = -1
  override var destinationHostAddress: String = _
  override var destinationHostName: String = _
  override var destinationPort: Int = -1

  override def extractProperties: Boolean =
    super.extractProperties && (for (
      // Check packet not null
      ethPkt <- Option(packet);
      // Get ip address
      dstAddress <- ethPkt.as[IpPacket].map(_.getHeader.getDstAddr); // If IP packet
      srcAddress <- ethPkt.as[IpPacket].map(_.getHeader.getSrcAddr); // If IP packet
      // Get port
      dstPort <- ethPkt.as[TcpPacket].map(_.getHeader.getDstPort) // If Tcp packet
        .orElse(ethPkt.as[UdpPacket].map(_.getHeader.getDstPort)); // If Udp packet
      srcPort <- ethPkt.as[TcpPacket].map(_.getHeader.getSrcPort) // If Tcp packet
        .orElse(ethPkt.as[UdpPacket].map(_.getHeader.getSrcPort)) // If Udp packet
    ) yield {
      destinationHostAddress = dstAddress.getHostAddress
      destinationHostName = Pcap4jAddressExtension.getCanonicalHostName(dstAddress)
      destinationPort = dstPort.valueAsInt
      sourceHostAddress = srcAddress.getHostAddress
      sourceHostName = Pcap4jAddressExtension.getCanonicalHostName(srcAddress)
      sourcePort = srcPort.valueAsInt
    }).isDefined
}
