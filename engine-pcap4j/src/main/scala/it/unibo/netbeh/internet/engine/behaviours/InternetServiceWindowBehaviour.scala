package it.unibo.netbeh.internet.engine.behaviours

import it.unibo.netbeh.core.behaviour.WindowBehaviour

case class InternetServiceWindowBehaviour(
                                    windowId: Int,
                                    behaviourList: Seq[InternetServiceBehaviour]
                                  ) extends WindowBehaviour[InternetServiceBehaviour] {

}
