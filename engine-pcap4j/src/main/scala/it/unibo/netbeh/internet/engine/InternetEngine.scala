package it.unibo.netbeh.internet.engine

import it.unibo.netbeh.core.behaviour.DeviceBehaviour
import it.unibo.netbeh.core.properties.extensions.{AddressExtension, TimeExtension}
import it.unibo.netbeh.internet.engine.behaviours.InternetAtomicBehaviour
import it.unibo.netbeh.internet.engine.modules.{Stage1, Stage2, Stage3}
import it.unibo.netbeh.internet.engine.properties.InternetProperties
import it.unibo.utils.Configuration
import rx.lang.scala.Observable

import scala.concurrent.ExecutionContext
import scala.concurrent.duration._
import scala.language.postfixOps

abstract class InternetEngine[P <: InternetProperties with TimeExtension with AddressExtension, T](localAddresses: Seq[String]) {

  private val windowSize: Duration = Configuration().getInt("behaviour.aggregator.window_size") seconds

  implicit val sm = InternetServiceManager

  def start(sniffer: Observable[T])(implicit ec: ExecutionContext) = {
    // properties extraction
    val st1 = stage1().run(sniffer)

    // behaviour extraction
    val st2 = stage2().run(st1)

    // behaviour aggregation
    val st3 = stage3().run(st2)

    st3
  }

  protected def stage1(): Stage1[T, P]

  private def stage2(): Stage2[P, InternetAtomicBehaviour] =
    Stage2[P, InternetAtomicBehaviour](
      localAddresses,
      (packet: P, outgoing) => InternetAtomicBehaviour(packet, outgoing)
    )

  private def stage3(): Stage3[InternetAtomicBehaviour, DeviceBehaviour[InternetAtomicBehaviour]] =
    Stage3[InternetAtomicBehaviour, DeviceBehaviour[InternetAtomicBehaviour]](
      windowSize,
      device => DeviceBehaviour[InternetAtomicBehaviour](device)
    )
}
