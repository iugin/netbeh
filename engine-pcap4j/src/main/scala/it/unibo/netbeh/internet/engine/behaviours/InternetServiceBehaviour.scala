package it.unibo.netbeh.internet.engine.behaviours

import com.fasterxml.jackson.annotation.{JsonIgnoreProperties, JsonProperty}
import it.unibo.netbeh.core.behaviour.ServiceBehaviour
import it.unibo.netbeh.core.behaviour.extensions.{CounterExtension, NeuralNetworkExtension}
import it.unibo.netbeh.internet.engine.InternetServiceManager

@JsonIgnoreProperties(ignoreUnknown = true)
case class InternetServiceBehaviour(
                                   serviceName: String,
                                   var behaviours: Seq[InternetAtomicBehaviour]
                                 ) extends ServiceBehaviour[InternetAtomicBehaviour]
  with CounterExtension
  with NeuralNetworkExtension {

  @JsonProperty override def avgIncomingLength: Double = super.avgIncomingLength

  @JsonProperty override def avgOutgoingLength: Double = super.avgOutgoingLength

  @JsonProperty override def maxIncomingLength: Int = behaviours.map(_.maxIncomingLength).max

  @JsonProperty override def minIncomingLength: Int = behaviours.map(_.minIncomingLength).min

  @JsonProperty override def totalIncomingLength: Long = behaviours.map(_.totalIncomingLength).sum

  @JsonProperty override def maxOutgoingLength: Int = behaviours.map(_.maxOutgoingLength).max

  @JsonProperty override def minOutgoingLength: Int = behaviours.map(_.minOutgoingLength).min

  @JsonProperty override def totalOutgoingLength: Long = behaviours.map(_.totalOutgoingLength).sum

  @JsonProperty override def nIncomingPackets: Long = behaviours.map(_.nIncomingPackets).sum

  @JsonProperty override def nOutgoingPackets: Long = behaviours.map(_.nOutgoingPackets).sum

  @JsonProperty def labels: Seq[String] = behaviours.flatMap(_.labels).distinct

  override def merge(atomicBehaviour: InternetAtomicBehaviour): Unit = ServiceBehaviour.merge(this, atomicBehaviour)

  override def serviceNumber: Int = InternetServiceManager.serviceNumber(this)
}

object InternetServiceBehaviour {

  implicit def toEngineServiceBehaviour(serviceBehaviours: ServiceBehaviour[InternetAtomicBehaviour]): InternetServiceBehaviour =
    InternetServiceBehaviour(serviceBehaviours.serviceName, serviceBehaviours.behaviours)

  implicit def toEngineServiceBehaviourSeq(serviceBehaviours: Seq[ServiceBehaviour[InternetAtomicBehaviour]]): Seq[InternetServiceBehaviour] =
    serviceBehaviours.map(toEngineServiceBehaviour)

}
