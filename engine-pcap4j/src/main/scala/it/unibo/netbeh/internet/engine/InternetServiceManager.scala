package it.unibo.netbeh.internet.engine

import java.util.regex.Pattern

import it.unibo.netbeh.core.behaviour.{Behaviour, DeviceBehaviour, ServiceBehaviour, ServiceManager}
import it.unibo.netbeh.internet.engine.behaviours.InternetAtomicBehaviour

object InternetServiceManager
  extends ServiceManager[InternetAtomicBehaviour, ServiceBehaviour[InternetAtomicBehaviour], DeviceBehaviour[InternetAtomicBehaviour]] {

  private final val patternCountry = Pattern.compile("\\[([a-zA-Z]{2})\\]$")

  private final val googleDomain = "1e100.net"
  private final val googleUserContentDomain = "googleusercontent.com"
  private final val facebookDomain = "facebook.com"
  private final val facebookCdn = "fbcdn.net"
  private final val messagenetDomain = "messagenet.it"
  private final val eoloDomain = "ngi.it"
  private final val amazonAwsDomain = "amazonaws.com"
  private final val timDomain = "tim.it"
  // Whois:
  private final val microsoft = "microsoft"
  private final val microsofNetName = "MSFT"
  private final val akaimaTechnologies = "akamaitechnologies.com"
  private final val cloudFront = "cloudfront.net"
  private final val openTelekomCloud = "open-telekom-cloud.com"

  private final var unknownDomains = Map[String, String]()

  /**
    * Return the service name
    * @param n the service
    * @return
    */
  private def getName(n: String): String = {
    if (n.endsWith(googleDomain)
      || n.endsWith(googleUserContentDomain)
      || n == "GOOGLE[US]"
    ) googleDomain                                              // If its part of the Google domain platform
    else if (n.endsWith(facebookDomain) || n.endsWith(facebookCdn)) facebookDomain  // If its part of the Facebook domain
    else if (n.endsWith(messagenetDomain)) messagenetDomain     // If its part of the Messagenet domain
    else if (n.endsWith(eoloDomain)) eoloDomain                 // If its part of the Eolo domain
    else if (n.endsWith(amazonAwsDomain)) amazonAwsDomain       // If its part of the Amazon AWS domain
    else if (n.endsWith(timDomain)
      || n == "TIM[IT]"
    ) timDomain                                                 // If its part of the TIM domain
    else if (n.contains(microsofNetName)) microsoft             // If its part of the Microsoft domain
    else if (n.endsWith(akaimaTechnologies)) akaimaTechnologies // If its part of the Akaima Technologies
    else if (n.endsWith(cloudFront)) cloudFront                 // If its part of the cloudFront
    else if (n.endsWith(openTelekomCloud)) openTelekomCloud     // If its part of the Open Telekom Cloud
    else {
      // Add to the log list of unknown domains
      // Group by country
      val m = patternCountry.matcher(n)
      if (m.find()) {
        val country = m.group(1)
        if (!unknownDomains.contains(n)) unknownDomains += (n -> country)
        country
      }
      // Otherwise in the global unknown bucket
      else {
        if (!unknownDomains.contains(n)) unknownDomains += (n -> "UNKNOWN")
        n
      }
    }
  }

  override def hasService(a: InternetAtomicBehaviour)(s: ServiceBehaviour[InternetAtomicBehaviour]): Boolean =
    serviceName(a) == s.serviceName

  override def serviceName(a: InternetAtomicBehaviour): String = s"${getName(a.serviceName)}:${a.servicePort}"

  /**
    * Neural network extension: return the discretized int value for the behaviour
    * @param a the atomic behaviour
    * @return the integer value
    */
  def serviceNumber(a: Behaviour): Int = a.serviceName.split(":").head match {
    case this.googleDomain => 0
    case this.facebookDomain => 1
    case this.messagenetDomain => 2
    case this.eoloDomain => 3
    case this.amazonAwsDomain => 4
    case this.timDomain => 5
    case this.microsoft => 6
    case this.akaimaTechnologies => 7
    case this.cloudFront => 8
    case this.openTelekomCloud => 9
    case "EU" => 10
    case "US" => 11
    case "GB" => 12
    case "IT" => 13
    case "NL" => 14
    case "DE" => 15
    case "CZ" => 16
    case _ => 19
  }

  final def getUnknownServices(): Map[String, String] = unknownDomains

  final def getAndResetUnknownServices(): Map[String, String] = {
    val res = unknownDomains
    unknownDomains = Map()
    res
  }
}
