package it.unibo.netbeh.internet.view

import it.unibo.fxviews.{FXInputChecks, FXInputViewController, FXViewController}
import it.unibo.netbeh.internet.engine.behaviours.InternetServiceBehaviour
import it.unibo.netbeh.utils.JavaFXUtils
import javafx.fxml.FXML
import javafx.scene.chart.{AreaChart, XYChart}
import javafx.scene.control.Slider
import javafx.scene.layout.{FlowPane, HBox}

/**
  * GUI layout in javaFX for the chart representation of the traffic
  *
  */
case class TrafficChartFXController(
                               ) extends FXViewController with FXChartController with FXInputViewController with FXInputChecks {

  @FXML private var chartBox: FlowPane = _
  @FXML private var scaleSlider: Slider = _

  override protected val layout: String = "/layouts/trafficChartLayout.fxml"

  override protected val title: String = "NetBeh - traffic graph"

  override protected val controller: FXViewController = this

  private val SERIES_UPLOAD = "Upload (MB)"
  private val SERIES_DOWNLOAD = "Download (MB)"

  private var networkTrafficChart: AreaChart[Number, Number] = _
  private var incomingNetworkProtocolChart: AreaChart[Number, Number] = _
  private var outgoingNetworkProtocolChart: AreaChart[Number, Number] = _

  override def resetFields(): Unit = {}

  override def disableViewComponents(): Unit = {}

  override def enableViewComponents(): Unit = {}

  override def initGUI(): Unit = {
    super.initGUI()
    scaleSlider.valueProperty.addListener((observable, oldValue, newValue) => {
      updateChartScale(newValue.doubleValue)
    })
    /*
     * Create the charts
     */
    // network traffic chart
    networkTrafficChart = newChart("Network traffic")
    chartMap += (networkTrafficChart.getTitle -> networkTrafficChart)
    // network protocol chart
    incomingNetworkProtocolChart = newChart("Network protocol - Incoming")
    chartMap += (incomingNetworkProtocolChart.getTitle -> incomingNetworkProtocolChart)
    outgoingNetworkProtocolChart = newChart("Network protocol - Outgoing")
    chartMap += (outgoingNetworkProtocolChart.getTitle -> outgoingNetworkProtocolChart)
    /*
     * Put the data in the axis
     */
    val seriesUpload = new XYChart.Series[Number, Number]()
    seriesUpload.setName(SERIES_UPLOAD)
    val seriesDownload = new XYChart.Series[Number, Number]()
    seriesDownload.setName(SERIES_DOWNLOAD)
    networkTrafficChart.getData.addAll(seriesUpload, seriesDownload)
    /*
     * Put the chart in the panel
     */
    chartBox.getChildren.add(new HBox(networkTrafficChart))
    chartBox.getChildren.add(new HBox(incomingNetworkProtocolChart))
    chartBox.getChildren.add(new HBox(outgoingNetworkProtocolChart))
    // Enable the tooltip for every chart
    JavaFXUtils.enableChartTooltip(networkTrafficChart)
    JavaFXUtils.enableChartTooltip(incomingNetworkProtocolChart)
    JavaFXUtils.enableChartTooltip(outgoingNetworkProtocolChart)
  }

  def update(window: Int, behaviours: Seq[InternetServiceBehaviour]): Unit = runOnUIThread(() => {
    /*
     * Update network traffic chart
     */
    val data = behaviours.foldLeft((0.0, 0.0))((r, b) => {
      (
        r._1 + b.totalOutgoingLength.toDouble/1000000,
        r._2 + b.totalIncomingLength.toDouble/1000000
      )
    })
    networkTrafficChart.getData.forEach((series: XYChart.Series[Number, Number]) => {
      series.getName match {
        case SERIES_UPLOAD => series.getData.add(new XYChart.Data(window,data._1))
        case SERIES_DOWNLOAD => series.getData.add(new XYChart.Data(window,data._2))
      }
    })
    updateChartVisualization(window, networkTrafficChart)
    /*
     * Update the network protocol chart
     */
    def getProtocolName(p: Int): String = p match {
      case 80 => "HTTP"
      case 443 => "HTTPS"
      case x => x.toString
    }
    def updateProtocolChart(chart: AreaChart[Number, Number], srcData: Map[String, Double]): Unit = {
      var data = srcData
      def updateSeries(series: XYChart.Series[Number, Number]): Unit = {
        if (data.contains(series.getName)) {
          series.getData.add(new XYChart.Data(window,data(series.getName)))
          data = data - series.getName
        }
      }
      // Update the existing series
      chart.getData.forEach(updateSeries)
      // Create a new series for each new port
      data.foreach(p => {
        val s = new XYChart.Series[Number, Number]()
        s.setName(p._1)
        chart.getData.add(s)
        updateSeries(s)
      })
      updateChartVisualization(window, chart)
    }
    // Incoming Protocol traffic
    updateProtocolChart(incomingNetworkProtocolChart, behaviours
      // Extract all atomic behaviours
      .flatMap(b => b.behaviours)
      // Sum all port traffic into one single value
      .foldLeft(Map[String, Double]())((r, b) => {
        if (!r.contains(getProtocolName(b.servicePort))) r + (getProtocolName(b.servicePort) -> b.totalIncomingLength)
        else r + (getProtocolName(b.servicePort) -> (b.totalIncomingLength + r(getProtocolName(b.servicePort))))
      }))
    // Outgoing Protocol traffic
    updateProtocolChart(outgoingNetworkProtocolChart, behaviours
      // Extract all atomic behaviours
      .flatMap(b => b.behaviours)
      // Sub all port traffic into one single value
      .foldLeft(Map[String, Double]())((r, b) => {
        if (!r.contains(getProtocolName(b.servicePort))) r + (getProtocolName(b.servicePort) -> b.totalOutgoingLength)
        else r + (getProtocolName(b.servicePort) -> (b.totalOutgoingLength + r(getProtocolName(b.servicePort))))
      }))
  })
}