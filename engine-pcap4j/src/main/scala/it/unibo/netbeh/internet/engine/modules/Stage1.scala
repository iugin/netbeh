package it.unibo.netbeh.internet.engine.modules

import it.unibo.netbeh.core.properties.PropertiesPacket
import it.unibo.netbeh.core.properties.extensions.{AddressExtension, TimeExtension}
import rx.lang.scala.Observable

case class Stage1[
  I,
  O <: PropertiesPacket with TimeExtension with AddressExtension
](
   builder: I => O
 ) extends it.unibo.netbeh.core.engine.Stage1[I, O] {

  override def run(input: Observable[I]): Observable[O] = Observable[O] { observer =>
    input.subscribe(o => {
      val pkt = builder(o)
      if (pkt.extractProperties) observer.onNext(pkt)
    })
  }
}
