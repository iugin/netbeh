package it.unibo.netbeh.internet.engine.behaviours

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.fasterxml.jackson.databind.node.ArrayNode
import com.fasterxml.jackson.databind.{DeserializationContext, JsonDeserializer, ObjectMapper}
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import it.unibo.netbeh.core.behaviour.{ExpectedComplexBehaviour, ExpectedPatternWindowBehaviour}

/**
  * Check that the sequence of ServiceBehaviour (of each window) respects the pattern expected.
  */
@JsonDeserialize(using = classOf[EngineExpectedServiceBehaviourDeserializer])
case class EngineExpectedServiceBehaviour(
                                           serviceName: String,
                                           patterns: Seq[ExpectedPatternWindowBehaviour[InternetServiceBehaviour, InternetExpectedServiceWindowBehaviour]]
                                         ) extends ExpectedComplexBehaviour[InternetServiceBehaviour, InternetExpectedServiceWindowBehaviour] {

  override def validate(behaviours: Seq[InternetServiceBehaviour]): Boolean =
  // Check that any pattern can match the behaviours sequence
    patterns.exists(matchPattern(behaviours, _))

  private def matchPattern(
                            behaviours: Seq[InternetServiceBehaviour],
                            pattern: ExpectedPatternWindowBehaviour[InternetServiceBehaviour, InternetExpectedServiceWindowBehaviour]
                          ): Boolean =
    if (behaviours.forall(_.serviceName==serviceName)) {
      if (pattern.options("order") == "sequential")
      // If ordered, matches the behaviours with the pattern expected in order.
      // The lists can differ in size, only the start is matched.
        behaviours.zip(pattern.sequence).forall { case (b, p) => p.isRespectedBy(b) }
      else false
    }
    else false
}

/**
  * Default jackson deserializer
  */
class EngineExpectedServiceBehaviourDeserializer extends JsonDeserializer[EngineExpectedServiceBehaviour] {

  override def deserialize(p: JsonParser, ctxt: DeserializationContext): EngineExpectedServiceBehaviour = {
    import com.fasterxml.jackson.databind.JsonNode
    val node: JsonNode = p.getCodec.readTree(p)

    import scala.collection.JavaConverters._
    val mapper = new ObjectMapper()
    mapper.registerModule(DefaultScalaModule)

    // Recreates the object by deserializing the unit objects one by one
    EngineExpectedServiceBehaviour(
      node.get("serviceName").asText(),
      node.get("patterns").asInstanceOf[ArrayNode].elements().asScala.map(n => ExpectedPatternWindowBehaviour[InternetServiceBehaviour, InternetExpectedServiceWindowBehaviour](
        n.get("sequence").asInstanceOf[ArrayNode].elements().asScala.map(p => mapper.readValue(p.toString, classOf[InternetExpectedServiceWindowBehaviour])).toSeq,
        mapper.readValue(n.get("options").toString, classOf[Map[String, String]])
      )).toSeq
    )
  }
}