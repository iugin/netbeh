package it.unibo.netbeh.internet.view

import it.unibo.fxviews.{FXInputChecks, FXInputViewController, FXViewController}
import javafx.fxml.FXML
import javafx.scene.control.{ChoiceBox, TextField}
import javafx.scene.layout.HBox

/**
  * GUI layout in javaFX for the main extraction application panel
  *
  * @param defaultMode the default mode of execution (optional)
  * @param defaultFilePath the default filepath for the 'file' execution mode (optional)
  * @param defaultDeviceIPs the default deviceIP list (separated by comma)
  * @param startCallback the callback called when the execution should start
  * @param stopCallback the callback called to stop the execution
  */
case class ExtractionFXController(
                                    defaultMode: String = "file",
                                    defaultFilePath: Option[String] = None,
                                    defaultDeviceIPs: String = "",
                                    startCallback: (String, Option[String], String) => Unit,
                                    stopCallback: () => Unit
                                  ) extends FXViewController with FXInputViewController with FXInputChecks {

  private val inModeLive = "live"
  private val inModeFile = "file"

  @FXML private var inMode: ChoiceBox[String] = _
  @FXML private var inFilePathBox: HBox = _
  @FXML private var inFilePath: TextField = _
  @FXML private var inDeviceIPs: TextField = _

  override protected def layout: String = "/layouts/extractionLayout.fxml"
  override protected def title: String = "NetBeh"
  override protected def controller: FXViewController = this

  override protected def initGUI(): Unit = {
    super.initGUI()
    inMode.getItems add inModeLive
    inMode.getItems add inModeFile
    inMode.getSelectionModel.selectedIndexProperty.addListener((value, n, n2) => inMode.getItems.get(n2.intValue) match {
      case `inModeLive` => inFilePathBox.setVisible(false)
      case `inModeFile` => inFilePathBox.setVisible(true)
    })
  }

  override def resetFields(): Unit = {
    inMode.setValue(defaultMode)
    inFilePath.setText(defaultFilePath.getOrElse(""))
    inDeviceIPs.setText(defaultDeviceIPs)
  }
  override def disableViewComponents(): Unit = { }
  override def enableViewComponents(): Unit = { }

  @FXML private def onClickStart(): Unit =
    runOnUIThread(() => {
      for (
        mode <- getChoiceBoxValue(inMode, "Invalid mode");
        deviceIPs <- getTextFieldValue(inDeviceIPs, "Invalid device IPs")
      ) yield mode match {
          case `inModeLive` =>
            startCallback(inModeLive, defaultFilePath, deviceIPs)
            showInfo("Valori selezionati", s"$inModeLive")
          case `inModeFile` =>
            getTextFieldValue(inFilePath, "Empty file path")
              .foreach(filepath => {
                startCallback(inModeFile, Option(filepath), deviceIPs)
                showInfo("Valori selezionati", s"$inModeFile - $filepath")
              })
      }
    })

  @FXML private def onClickStop(): Unit =
    runOnUIThread(() => {
      stopCallback()
      showInfo("Azione", "Stop")
    })
}