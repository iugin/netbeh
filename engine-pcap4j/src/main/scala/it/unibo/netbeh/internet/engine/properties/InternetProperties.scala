package it.unibo.netbeh.internet.engine.properties

import it.unibo.netbeh.core.properties.PropertiesPacket
import it.unibo.netbeh.core.properties.extensions.{AddressExtension, CounterExtension, TimeExtension}

trait InternetProperties extends PropertiesPacket with AddressExtension with CounterExtension with TimeExtension {

  /**
    * Neural Network training labels
    */
  def label: String
}
