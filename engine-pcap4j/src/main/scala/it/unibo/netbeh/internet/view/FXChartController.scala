package it.unibo.netbeh.internet.view

import it.unibo.fxviews.FXViewController
import it.unibo.utils.Configuration
import javafx.scene.chart.{AreaChart, NumberAxis, XYChart}

trait FXChartController extends FXViewController {

  private val MAX_DATA_POINTS = Configuration().getInt("gui.chart.history")
  private val DEFAULT_CHART_HEIGHT = Configuration().getInt("gui.chart.height")
  private val DEFAULT_CHART_WIDTH = Configuration().getInt("gui.chart.width")

  private var chartScale = 1.0
  protected var chartMap: Map[String, AreaChart[Number, Number]] = Map[String, AreaChart[Number, Number]]()

  /**
    * Updates the size of the chart and its title according to the updated chartScale value
    * @param chart the chart to update to the new size
    */
  private def updateChartSize(chart: AreaChart[Number, Number]): Unit = {
    chart.setStyle(
      s"""
         |-fx-font-size: ${chartScale*1.5}em;
         |""".stripMargin)
    chart.setPrefHeight(Math.round(DEFAULT_CHART_HEIGHT * chartScale))
    chart.setPrefWidth(Math.round(DEFAULT_CHART_WIDTH * chartScale))
  }

  /**
    * Updates the scale value for the chart visualization
    * @param scale the new scale value
    */
  protected def updateChartScale(scale: Double): Unit = {
    if (scale != chartScale) {
      chartScale = scale
      chartMap.values.foreach(updateChartSize)
    }
  }

  /**
    * Returns a newly created chart with the given title
    * @param title the title of the chart
    * @return the new chart
    */
  protected def newChart(title: String): AreaChart[Number,Number] = {
    var xAxis = new NumberAxis(0, MAX_DATA_POINTS, MAX_DATA_POINTS / 10)
    var yAxis = new NumberAxis()
    val ac = new AreaChart[Number,Number](xAxis,yAxis)
    ac.setTitle(title)
    updateChartSize(ac)
    ac
  }

  /**
    * Updates the data visualization in the chart:
    * - slides all the data
    * - updates the x axis to fit the data
    * @param x the x value, last added
    * @param ac the AreaChart to update
    */
  protected def updateChartVisualization(x: Int, ac: AreaChart[Number, Number]): Unit = {
    ac.getData.forEach(series => {
      // Remove exceeding points
      if (series.getData.size() > MAX_DATA_POINTS) {
        series.getData.remove(0, series.getData.size() - MAX_DATA_POINTS)
      }
    })
    // Update xAxis
    ac.getXAxis.asInstanceOf[NumberAxis].setLowerBound(x - MAX_DATA_POINTS + 1)
    ac.getXAxis.asInstanceOf[NumberAxis].setUpperBound(x)
  }

  /**
    * Create and add a new zero-data-point in the chart
    * @param x the x value into which to put the zero value
    * @param ac the AreaChart to update
    */
  protected def addZerosToChart(x: Int, ac: AreaChart[Number, Number]): Unit = {
    ac.getData.forEach(series => {
      if (series.getData.size == 0 || series.getData.get(series.getData.size-1).getXValue.intValue != x)
        series.getData.add(new XYChart.Data(x,0))
    })
  }
}
