package it.unibo.netbeh.internet.pcap4j

import java.sql.Timestamp

import it.unibo.netbeh.core.properties.extensions.TimeExtension
import it.unibo.netbeh.internet.pcap4j.properties.{Pcap4jAddressExtension, Pcap4jCounterExtension, Pcap4jPropertiesPacket}
import org.pcap4j.packet.Packet

case class Pcap4JProperties(
                             override val packet: Packet,
                             override val timestamp: Timestamp,
                             label: String)
  extends Pcap4jPropertiesPacket(packet) with Pcap4jAddressExtension with Pcap4jCounterExtension with TimeExtension
