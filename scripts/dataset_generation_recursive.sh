#!/bin/sh

PCAP_DIR=$1 # input folder
LOCAL_IP=$2 # ip to inspect
DATASET_DESTINATION=$3 # output directory

SCRIPT="$(dirname $0)/dataset_generation.sh"
echo $SCRIPT
echo $PWD

for FILE in $(find $PCAP_DIR -type f -name "*.pcap"); do
    [ -f "$FILE" ] || break
    LOG_FILE="$FILE.log"
    COMMAND="$SCRIPT $FILE $LOCAL_IP $DATASET_DESTINATION"
    echo "--------------- Generating dataset for ---------------------"
    echo "Source: 	$FILE"
    echo "Log: 			$LOG_FILE"
    echo "Command:	$COMMAND"
    echo "------------------------------------------------------------"
    $COMMAND > $LOG_FILE
    if [ $? -eq 0 ]; then
				echo "Dataset generated!"
		else
				echo "Dataset generation failed."
				exit 1
		fi
done

