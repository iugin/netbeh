#!/bin/bash

# Tools
LABELS_GENERATOR="stage0-labeling/example/ndpiReader"

# Engine jar
PREPROCESSING_JAR="engine-pcap4j/build/libs/engine-pcap4j-1.0-SNAPSHOT-all.jar"

# Stage4 NN
NN_VENV="venv"
NN_APPLICATION="stage4-inferring/src/Application_live.py"
