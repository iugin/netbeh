#!/bin/bash

##############################################################
# Generates the dataset and moves it to the correct location.
# It also generates all the intermediate files
##############################################################

# Parameters
PCAP_FILE=$1 # input file
LOCAL_IPs=$2 # ip to inspect
DATASET_DESTINATION=$3 # output directory

# Load configuration
source "$(dirname $0)/configuration.sh"

# Generated parameters
DIRECTORY=$(dirname "${PCAP_FILE}")	# Main directory
PCAP_NAME=$(basename "${PCAP_FILE}")	# Name of the PCAP file
PCAP_NAME="${PCAP_NAME%.*}"			# Name of the PCAP file (no extension)

# Generate labels
echo "Generating labels file..."
$LABELS_GENERATOR -i $PCAP_FILE
if [ $? -eq 0 ]; then
    echo "Generation of labels successful!"
else
    echo "Generation of labels failed."
    exit 1
fi

# Preprocess packets
echo "Generating dataset file..."
java -jar $PREPROCESSING_JAR --no-gui -i "${PCAP_FILE%.*}" -ip $LOCAL_IPs
if [ $? -eq 0 ]; then
    echo "Generation of dataset successful!"
else
    echo "Generation of dataset failed."
    exit 1
fi

# Move dataset
echo "Moving dataset file..."

IFS=',' read -ra local_ip_list <<< "$LOCAL_IPs"

#Print the split string
for local_ip in "${local_ip_list[@]}"
do
		echo "Moving $DATASET_NAME"
		DATASET_NAME="${DIRECTORY}/${PCAP_NAME}/${local_ip}-NN.csv"
		DESTINATION_PATH="${DATASET_DESTINATION}/${local_ip}"
		DESTINATION_FILE="${DESTINATION_PATH}/${PCAP_NAME}.csv"

		mkdir -p $DESTINATION_PATH
		cp $DATASET_NAME $DESTINATION_FILE
		if [ $? -eq 0 ]; then
				echo "Dataset moved to $DESTINATION_FILE!"
		else
				echo "Move failed of $DATASET_NAME."
		fi
done


