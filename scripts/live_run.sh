#!/bin/bash

### ARGUMENTS
NETWORK_MODEL_PATH=$1 # path to the NN model
LOCAL_IPs=$2 # ip to inspect
N_INTERFACE=$3 # input file
JAVA_PATH=$4 # java path

# Load configuration
source "$(dirname $0)/configuration.sh"

# PARAMETERS
JAVA_EXEC=$JAVA_PATH/bin/java

if [[ -z ${N_INTERFACE} ]]
then
    INTERMEDIATE_NN_FOLDER="live"
else
    INTERMEDIATE_NN_FOLDER="${N_INTERFACE}-live"
fi
mkdir $INTERMEDIATE_NN_FOLDER

# Launch the NN evaluation
echo "Starting python..."
source $NN_VENV/bin/activate
python $NN_APPLICATION $INTERMEDIATE_NN_FOLDER $NETWORK_MODEL_PATH &
PID1=$!
echo "Running as $PID1"

# Add permission to sniff
echo "Adding sniffing capabilities..."
sudo setcap cap_net_raw,cap_net_admin=eip $JAVA_EXEC

# Start preprocessing the raw packets trough the first stages of the engine
echo "Starting preprocessing engine..."
$JAVA_EXEC -jar $PREPROCESSING_JAR --no-gui -d $N_INTERFACE -ip $LOCAL_IPs &
PID2=$!
echo "Running as $PID2"

function terminationHandler() {
	kill $PID1
	kill $PID2
	echo "All processes terminated! ($PID1, $PID2)"

	# Remove permission to sniff
	echo "Removing sniffing capabilities..."
	sudo setcap -r $JAVA_EXEC
}

#trap terminationHandler EXIT
trap terminationHandler INT
wait
